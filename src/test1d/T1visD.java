package test1d;

import java.text.*;

public class T1visD {	//	T1vis用の構造化されたデータ
	double[] T;
	double[][] d;		//	数値データ本体
	double tmin,tmax;	//	最小、最大値
	double tmin0,tmax0;	//	デフォルト値
	T1visD(){}
	T1visD(String filename, double[] T, double[][] d){
		this.d=d;
		this.T=T;
		analyze();
		System.out.println(filename+"\t"+d[0].length+"\t"+new DecimalFormat("0.000E00").format(tmin)
				+"\t"+new DecimalFormat("0.000E00").format(tmax));
	}
	void analyze() {	//	最大、最小値を求める。グラフを正規化するためのもの
		tmin=1.0e+99;
		tmax=-1.0e+99;
		for (int i=0; i<d.length; i++) {
			for (int j=0; j<d[i].length; j++) {
				tmin=Math.min(tmin,d[i][j]);
				tmax=Math.max(tmax,d[i][j]);
			}
		}
		tmin0=tmin;
		tmax0=tmax;
	}
	double[][] normalize() {
		double max=Math.max(Math.abs(tmin),tmax);
		double[][] y=new double[d.length][d[0].length];
		for (int i=0; i<d.length; i++) {
			for (int j=0; j<d[i].length; j++) {
				y[i][j]=d[i][j]/max;
			}
		}
		return y;
	}
	double[][] average(){	//	 20x50の配列に平均化する
		int nd=20,nt=500;
		int ni=10000/nt;
		double[][] x=normalize();
		double[][] y=new double[nt][nd];
		for (int j=0; j<d[0].length; j++) {
			for (int i=0; i<d.length; i++) {
				int n=i/ni;
				if (n<500) y[n][j]+=x[i][j]/(double)ni;
			}	
		}
		return y;
	}
	double[][] max_inten(){		//	200タイムステップ中の絶対値の最大
		int nd=20,nt=500;
		int ni=10000/nt;		
		double[][] x=normalize();
		double[][] y=new double[nt][nd];
		for (int j=0; j<d[0].length; j++) {
			for (int n=0; n<nt; n++) {
				int imin=n*ni;
				int imax=(n+1)*ni;
				double dmax=0.0;
				for (int i=imin; i<imax; i++) {
					dmax=Math.max(dmax, Math.abs(x[i][j]));
				}
				y[n][j]=dmax;
			}
		}
		return y;
	}
	void set_minmax(double dmin, double dmax) {
		if (dmin==0.0 && dmax==0.0) {
			tmin=tmin0;
			tmax=tmax0;
		}
		else {
			tmin=dmin;
			tmax=dmax;
		}
	}
	String get_minmaxstr() {
		String str="min="+new DecimalFormat("0.000E00").format(tmin)
				+" max="+new DecimalFormat("0.000E00").format(tmax);
		return str;
	}
	double[] getT() { return T; }
	int get_nd() { return d[0].length; }
}
