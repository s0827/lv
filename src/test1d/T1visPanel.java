package test1d;

import java.awt.*;

public abstract class T1visPanel extends Panel {		//	電流の線グラフ
	int panelwidth=500,panelheight=200;
	int margin=50;
	String label;
	T1visD D;
	T1visPanel(String label){
		setSize(panelwidth,panelheight+margin);
		setVisible(true);
		this.label=label;
	}
	void setD(T1visD D) { this.D=D; }
	void draw_label(Graphics gc) {
		gc.setColor(Color.black);
		String str=label+": "+D.get_minmaxstr();
		gc.drawString(str,10,230);
	}
	int limit(int x) {
		if (x<0) {
			x=0;
		}
		else if (x>255) {
			x=255;
		}
		return x;
	}
	abstract Color get_color(double d);
}
