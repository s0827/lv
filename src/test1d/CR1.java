package test1d;

import org.apache.commons.math.linear.Array2DRowRealMatrix;
import org.apache.commons.math.linear.ArrayRealVector;
import org.apache.commons.math.linear.DecompositionSolver;
import org.apache.commons.math.linear.LUDecompositionImpl;
import org.apache.commons.math.linear.RealMatrix;
import org.apache.commons.math.linear.RealVector;
import java.text.*;

public class CR1 {
	int NY=10;
	double R=30.0;
	double C=3.0e-13;
	double L=1.0e-15;
	double Vp=1.0e+6;
	double dt=1.0e-13;
	double[] Q,Q0,Q1,V,V0;
	int Ny=10;
	public static void main(String args[]){
		CR1 cr=new CR1();
	}
	CR1(){
		Q=initialize(Ny);
		Q0=initialize(Ny);
		Q1=initialize(Ny);
		V=initialize(Ny-1);
		V0=initialize(Ny-1);
		
		double time=0.0;
		while(time<1.0e-11){
			calculate();
			time+=dt;
			System.out.print(new DecimalFormat("0.00E00").format(time));
			for (int iy=1; iy<Ny; iy++){
				System.out.print("\t"+new DecimalFormat("0.000E00").format(V[jm1(iy)]));
			}
			System.out.println("\t"+new DecimalFormat("0.000E00").format(current()));
		}		
	}
	double[] initialize(int n){
		double[] X=new double[n];
		for (int iy=0; iy<X.length; iy++) X[iy]=0.0;
		return X;
	}
	void calculate(){
		RealMatrix a;
		RealVector b;

		double P=2.0*R/dt+1.0/C;
		double M=2.0*R/dt-1.0/C;
		
		int num=2*Ny-1;
		a=new Array2DRowRealMatrix(num,num);
		b=new ArrayRealVector(num);
		shift();

		int ieq=0;
		for (int iy=1; iy<Ny; iy++){
			a.setEntry(ieq,nQ(iy-1),-1.0);
			a.setEntry(ieq,nQ(iy),1.0);
			b.setEntry(ieq,Q0[iy-1]-Q0[iy]);
			ieq++;
		}
		for (int iy=1; iy<Ny+1; iy++){
			if (iy==1){
				a.setEntry(ieq,nV(iy),-1.0);
				a.setEntry(ieq,iy-1,P);
				b.setEntry(ieq,V0[jm1(iy)]+M*Q0[iy-1]);
			}
			else if (iy==Ny) {
				a.setEntry(ieq,nV(iy-1),1.0);
				a.setEntry(ieq,nQ(iy-1),P);
				b.setEntry(ieq,2.0*Vp-V0[jm1(iy-1)]+M*Q0[iy-1]);							
			}
			else {
				a.setEntry(ieq,nV(iy),-1.0);
				a.setEntry(ieq,nV(iy-1),1.0);
				a.setEntry(ieq,nQ(iy-1),P);
				b.setEntry(ieq,V0[jm1(iy)]-V0[jm1(iy-1)]+M*Q0[iy-1]);				
			}
			ieq++;
		}
//		dump(a,b,null);
		
		DecompositionSolver solver = new LUDecompositionImpl(a).getSolver();
		RealVector x=solver.solve(b);
		
		get_result(x);
	}
	void get_result(RealVector x){
		for (int iy=0; iy<Ny; iy++) Q[iy]=x.getEntry(nQ(iy));
		for (int iy=1; iy<Ny; iy++) V[jm1(iy)]=x.getEntry(nV(iy));
	}
	void shift(){
		shift(V,V0);
		shift(Q0,Q1);
		shift(Q,Q0);
	}
	void shift(double[] X, double[] X0){
		for (int iy=0; iy<X.length; iy++) X0[iy]=X[iy];
	}
	double current(){ return (Q[0]-Q0[0])/dt; }
	
	void dump(RealMatrix a, RealVector b, RealVector x){
		int num=2*Ny-1;
		for (int i=0; i<num; i++){
			for (int j=0; j<num; j++){
				System.out.print(df(a.getEntry(i,j))+"\t");
			}
			System.out.print(df(b.getEntry(i))+"\t");
			if (x!=null) System.out.print(df(x.getEntry(i)));
			System.out.println("");
		}
	}
	String df(double x){
		String s=new String(".");
		if (x!=0.0) {
			s=new DecimalFormat("0.000E00").format(x);
			if (x>=0) s=" "+s;			
		}
		return s;
	}

	int nV(int iy){ return jm1(iy); }
	int nQ(int iy){ return Ny-1+iy; }
	int jm1(int iy){ return iy-1; }
}
