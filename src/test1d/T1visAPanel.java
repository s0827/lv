package test1d;

import java.awt.*;

public class T1visAPanel extends T1visPanel {			//�@���ϒl�̕`��
	T1visAPanel(String label){
		super(label);
	}
	public void paint(Graphics gc) {
		if (D==null) return;
		gc.setColor(Color.white);
		gc.fillRect(0,0,panelwidth, panelheight);
		double[][] y=D.average();
		for (int i=0; i<y.length; i++) {
			for (int j=0; j<y[i].length; j++) {
				int iy0=panelheight-j*10;
				int iy1=iy0-10;
				Color c=get_color(y[i][j]);
				gc.setColor(c);
				gc.drawLine(i, iy0, i, iy1);
			}
		}
		draw_label(gc);
	}
	Color get_color(double d) {
		int r=0,g=0,b=0;
		if (d>0.0) {
			r=(int)(255.0*d);
			r=limit(r);
		}
		else {
			b=(int)(255.0*(-d));
			b=limit(b);
		}
		Color c=new Color(r,g,b);
		return c;
	}
}
