package test1d;

import org.apache.commons.math.linear.*;

public class T1dcellN extends T1dcell {
	double Vglogbal, Rglobal;
	T1dcellN(){}
	T1dcellN(int n, double Vglobal, double Rglobal){ 
		super(n);
		this.Vglobal=Vglobal;
		this.Rglobal=Rglobal;
	}
	void set_adjacent(T1dcell cm, T1dcell cp) {
		this.cm=cm;
	}
	void set_coefficients(RealMatrix a, RealVector b) {
		calc_Qeq(a,b);
		
		a.setEntry(i2+1,i2-1,-0.5*Rglobal/R);
		a.setEntry(i2+1,i2  ,Rglobal/dt);
		a.setEntry(i2+1,i2+1,0.5*(1.0+Rglobal/R));
		b.setEntry(i2+1,Vglobal+0.5*Rglobal/R0*cm.getV0()
				+Rglobal/dt*Q0-0.5*(1.0+Rglobal/R0)*V0);
	}
	void get_qv(RealVector x){
		Q=x.getEntry(i2);
		V=x.getEntry(i2+1);
	}
}
