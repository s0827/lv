package test1d;

import java.io.*;
import java.util.*;
import java.text.*;
import java.awt.*;

public class T1vis extends Frame {	//	T1dの結果の可視化
	double[] T=null;
	T1visD E,R,I;	
	int framewidth=600;
	int frameheight=800;
	T1visPanel epanel,rpanel,ipanel;
	int wl=40,hl=40,hi=250;				//	線パネルの位置
	
	public static void main(String args[]) {
		T1vis t=new T1vis();
	}
	T1vis(){
		R=read_file("rfile");
		E=read_file("efile");
		I=read_file("ifile");
		output_(I);					//	他のソフトでの描画のために出力
		init_graphics();
		process();					//	メインイベントループ
	}
	T1visD read_file(String filename){
		LinkedList<String> L=new LinkedList<String>();
		try {
			BufferedReader in=new BufferedReader(new FileReader(filename+".txt"));
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				L.add(line);		//	データをリストの格納
			}
			in.close();
		}
		catch(Exception e) {
			System.err.println(e);
		}
		if (T==null) get_T(L);		//	最初のファイルから時刻を読む
	
		String[] fstrs=L.getFirst().split("\t");
		double[][] d=null;
		d=new double[T.length][fstrs.length-1];
		int i=0;
		for (String l: L) {
			String[] strs=l.split("\t");
			for (int j=0; j<d[i].length; j++) d[i][j]=Double.parseDouble(strs[j+1]);
			i++;
		}
		T1visD D=new T1visD(filename,T,d);
		return D;
	}
	void get_T(LinkedList<String> L) {
		T=new double[L.size()];
		int i=0;
		for (String l: L) {
			String[] strs=l.split("\t");
			T[i++] = Double.parseDouble(strs[0]);
		}
		System.out.println(T.length+" timesteps");
	}
	void init_graphics() {
		setSize(framewidth,frameheight);
		setVisible(true);
		setLayout(null);
		
		epanel=new T1visAPanel("Efield");
		epanel.setLocation(wl,hl);
		epanel.setD(E);
		add(epanel);		
		hl+=hi;
		
		rpanel=new T1visAPanel("Resistance");
		rpanel.setLocation(wl,hl);
		rpanel.setD(R);
		add(rpanel);
		hl+=hi;
		
		ipanel=new T1visIPanel("Discharge Current");
		ipanel.setLocation(wl,hl);
		ipanel.setD(I);
		add(ipanel);
	}
	void output_(T1visD I) {
		double[][] y=I.normalize();
		for (int i=0; i<y.length; i++) {
			for (int j=0; j<y[i].length; j++) {
				y[i][j]+=(double)j;
			}
		}
		double[] T=I.getT();
		try {
			PrintWriter out=new PrintWriter(new BufferedWriter(new FileWriter("igf.txt")));
			out.print("time");
			for (int j=0; j<I.get_nd(); j++) out.print("\t"+j);
			out.println();
			for (int i=0; i<y.length; i++) {
				out.print(new DecimalFormat("0.000E00").format(T[i]));
				for (int j=0; j<y[i].length; j++) {
					out.print("\t"+new DecimalFormat("0.000E00").format(y[i][j]));
				}
				out.println();
			}
			out.close();
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	void process() {
		try {
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			while(true) {
				System.out.print("Item min max > ");
				String line=in.readLine();
				String[] strs=line.split(" ");
				double dmin=0.0;
				double dmax=0.0;
				if (strs.length==3) {
					dmin=Double.parseDouble(strs[1]);
					dmax=Double.parseDouble(strs[2]);
				}
				if (strs[0].toUpperCase().equals("E")) {
					E.set_minmax(dmin,dmax);
					epanel.repaint();
				}
				else if (strs[0].toUpperCase().equals("R")) {
					R.set_minmax(dmin,dmax);
					rpanel.repaint();
				}
				else if (strs[0].toUpperCase().equals("I")) {
					I.set_minmax(dmin,dmax);
					ipanel.repaint();
				}
			}
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	public void paint(Graphics gc) {
		gc.setColor(Color.white);		
		gc.fillRect(0,0,framewidth,frameheight);
		if (epanel!=null) epanel.repaint();
		if (rpanel!=null) rpanel.repaint();
		if (ipanel!=null) ipanel.repaint();
	}
}
