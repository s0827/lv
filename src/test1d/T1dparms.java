package test1d;

import java.io.*;
import java.util.*;
import java.text.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class T1dparms {
	double dx=0.0, dy=0.0, dz=0.0;			//	cell size
	int nz=0;								// 	number of meshes (z-axis)
	double rz=0.0;							//  length of calculation region (z-axis)
	double rho0=1.0e+4, rratio=1.0e+4;
	boolean pres_scaling_f=false;
	double Vglobal=3.0e+5;					//	global voltage [V]
	double Rglobal=300.0;					//	global resistance
	double etaon=1.0e+00, etaoff=1.0e+04;	//	resistivity (on / off)
	double C,ron=0.0, roff=0.0;
	double maxtime=1.1e-10, timestep=1.0e-12;
	boolean charged_f=false;				//	if true capacitors are charged
	double prate=5.0e-12;					//  at ref_efield, percolation occurs at prate
	double ref_efield=1.0e+6;				//	
	double ef_mult=0.0;						//	rate scales as (ef/ref_efield)^e_mult
	double[] relative_pres=null;			//	relative pressure of each cell
	double[] current=null;					//	current supplied at each node
	int seed=0;
	
	public T1dparms(String path){
		try {
			InputStream is=new FileInputStream(path);
			DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
			DocumentBuilder builder=factory.newDocumentBuilder();
			Document doc=builder.parse(is);
			Element rootElement=doc.getDocumentElement();
			NodeList x_list=rootElement.getElementsByTagName("x");				//	get mesh information
			dx=Double.parseDouble(get_attr(x_list,"dx"));
			NodeList y_list=rootElement.getElementsByTagName("y");
			dy=Double.parseDouble(get_attr(y_list,"dy"));
			NodeList z_list=rootElement.getElementsByTagName("z");
			dz=Double.parseDouble(get_attr(z_list,"dz"));
			nz=Integer.parseInt(get_attr(z_list,"num"));

			NodeList resistivity_list=rootElement.getElementsByTagName("resistivity");
			if (resistivity_list!=null) {
				rho0=Double.parseDouble(get_attr(resistivity_list,"rho0"));
				rratio=Double.parseDouble(get_attr(resistivity_list,"on_off_ratio"));
				pres_scaling_f=false;
				if (get_attr(resistivity_list,"pres_scaling").equals("true")) {
					pres_scaling_f=true;
				}
			}
			NodeList global_circuit_list=rootElement.getElementsByTagName("global_circuit");
			if (global_circuit_list!=null) {
				Vglobal=Double.parseDouble(get_attr(global_circuit_list,"Vglobal"));
				Rglobal=Double.parseDouble(get_attr(global_circuit_list,"Rglobal"));
			}
			NodeList cell_list=rootElement.getElementsByTagName("cell_data");
			if (cell_list!=null) {
				relative_pres=new double[cell_list.getLength()];
				current=new double[cell_list.getLength()];
				for (int i=0; i<cell_list.getLength(); i++) {
					Node cell_node=cell_list.item(i);
					NamedNodeMap cell_map=cell_node.getAttributes();
					double z=Double.parseDouble(cell_map.getNamedItem("z").getNodeValue());	//	altitude not used (tentative)
					double p=Double.parseDouble(cell_map.getNamedItem("p").getNodeValue());
					double j=Double.parseDouble(cell_map.getNamedItem("j").getNodeValue());
					relative_pres[i]=p;
					current[i]=j;
				}
			}
			NodeList calc_list=rootElement.getElementsByTagName("calc");
			maxtime=Double.parseDouble(get_attr(calc_list,"maxtime"));
			timestep=Double.parseDouble(get_attr(calc_list,"timestep"));
			
			NodeList initial_condition_list=rootElement.getElementsByTagName("initial_condition");
			if (get_attr(initial_condition_list,"charged").equals("true")) {
				charged_f=true;
			}
			NodeList perc_list=rootElement.getElementsByTagName("percolation");
			prate=Double.parseDouble(get_attr(perc_list,"prate"));
			ref_efield=Double.parseDouble(get_attr(perc_list,"ref_efield"));
			ef_mult=Double.parseDouble(get_attr(perc_list,"ef_mult"));
			
			NodeList random_list=rootElement.getElementsByTagName("random");
			seed=Integer.parseInt(get_attr(random_list,"seed"));
			
			is.close();
			rz=dz*(double)(nz);
			C=8.854e-12/dz;
			roff=rho0*dz;
			ron=roff/rratio;
		}		
		catch(Exception e){
			System.err.println(e);
		}
	}
	String get_attr(NodeList nlist, String str){
		Node x_node=nlist.item(0);
		NamedNodeMap attr=x_node.getAttributes();
		Node item=attr.getNamedItem(str);
		if (item==null) return null;
		return item.getNodeValue();
	}
	double getDx(){ return dx; }
	double getDy(){ return dy; }
	double getDz(){ return dz; }
	double getRz(){ return rz; }
	int getNz(){ return nz; }
	double getRon(){ return ron; }
	double getRoff(){ return roff; }
	double getVglobal(){ return Vglobal; }
	double getRglobal(){ return Rglobal; }
	double getC(){ return C; }
	double getMaxtime(){ return maxtime; }
	double getTimestep(){ return timestep; }
	boolean isCharged(){ return charged_f; }
	double getPrate(){ return prate; }
	double getRef_efield(){ return ref_efield; }
	double getEf_mult() { return ef_mult; }
	double[] getRelative_pres() { return relative_pres; }
	double[] getCurrent() { return current ; }
	int getSeed(){ return seed; }
	boolean isPres_scaling() { return pres_scaling_f; }
}
