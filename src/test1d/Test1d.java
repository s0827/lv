package test1d;

import org.apache.commons.math.linear.*;
import java.text.*;
import java.io.*;

public class Test1d {
	T1dparms parms;
	T1dcell[] cells;
	T1dbreakdown breakdown;
	double Rglobal;
	RealMatrix a;
	RealVector b;
	PrintWriter vfile,rfile,ifile;
	double[] pQ;	//	Q at previous step for calculation of current
	double dt;
	
	public static void main(String args[]){
		Test1d t1d=new Test1d();
	}
	Test1d(){
		parms=new T1dparms("T1d_input_lv.xml");
		setup();
		calculate();
		breakdown.close();
	}
	void setup(){
		double Vglobal=parms.getVglobal();			//	Global voltage
		Rglobal=parms.getRglobal();
		int nz=parms.getNz();		
		cells=new T1dcell[nz];
		cells[0]=new T1dcell0(0);
		cells[nz-1]=new T1dcellN(nz-1,Vglobal,Rglobal);
		for (int n=1; n<nz-1; n++) {				//	initialize cell
			cells[n]=new T1dcelli(n);
		}
		for (int n=0; n<nz; n++) {					//	define adjacent cells
			T1dcell cm=null;	
			T1dcell cp=null;
			if (n>0) cm=cells[n-1];
			if (n<nz-1) cp=cells[n+1];
 			cells[n].set_adjacent(cm,cp);
		}
		
		double Vinitial=0.0;
		if (parms.isCharged()) Vinitial=Vglobal;
		set_initial_voltage(Vinitial);
		
		breakdown=new T1dbreakdown(cells,parms);	//	find the cell to cause breakdown

		int num=nz*2;								//	 2N+1 elements are required to calculate VN
		a=new Array2DRowRealMatrix(num,num);
		b=new ArrayRealVector(num);
	}
	void set_initial_voltage(double Vinitial) {
		int nz=parms.getNz();		
		double[] R=new double[nz];
		double[] Rc=new double[nz];
		double[] V=new double[nz+1];
		double[] Q=new double[nz];
		double Rtotal=0.0;
		double C=parms.getC();
		for (int n=0; n<nz; n++) {
			R[n]=parms.getRoff();
			if (parms.isPres_scaling()) R[n]=R[n]*parms.getRelative_pres()[n];
			Rtotal+=R[n];
			Rc[n]=Rtotal;
		}
		double Rcircuit=Rtotal;
		Rtotal+=Rglobal;
		V[0]=0.0;
		V[nz-1]=Vinitial*Rcircuit/Rtotal;
		for (int n=0; n<nz-1; n++) {
			V[n]=Vinitial*Rc[n]/Rtotal;
		}
		for (int n=0; n<nz; n++) {
			double Vm=0.0;
			if (n>0) Vm=V[n-1];
			Q[n]=(V[n]-Vm)*C;
		}
		System.out.println("initial values: C= "+new DecimalFormat("0.000E00").format(C));
		System.out.println("#\tV[V]\tR[Ohms]\tQ[C]\tQ/C[V]\ttau[s]");
		for (int n=0; n<nz; n++){
			cells[n].set(R[n], C,Q[n],V[n],parms.getCurrent()[n+1]);
			System.out.println(n+"\t"+new DecimalFormat("0.000E00").format(V[n])
					+"\t"+new DecimalFormat("0.000E00").format(R[n])
					+"\t"+new DecimalFormat("0.000E00").format(Q[n])
					+"\t"+new DecimalFormat("0.000E00").format(Q[n]/C)
					+"\t"+new DecimalFormat("0.000E00").format(R[n]*C));

		}
	}
	void calculate(){
		double time=0.0;
		dt=parms.getTimestep();
		try {
			vfile=new PrintWriter(new BufferedWriter(new FileWriter("vfile.txt")));
			rfile=new PrintWriter(new BufferedWriter(new FileWriter("rfile.txt")));
			ifile=new PrintWriter(new BufferedWriter(new FileWriter("ifile.txt")));
			output(time,cells,0.0,0.0);		//vfile and rfile are class variable
			while(true){
				time+=dt;
				for (int n=0; n<cells.length; n++) cells[n].setDt(dt);
				for (int n=0; n<cells.length; n++) cells[n].shift();
					
				T1dcell cell=breakdown.get_next_cell(time);
				if (cell!=null) cell.setR(parms.getRon());
				
				calcQV();
				
				double ir=cells[cells.length-1].getV()/cells[cells.length-1].getR();
				double ic=(cells[cells.length-1].getQ()-cells[cells.length-1].getQ0())/dt;
/*				System.out.print(new DecimalFormat("0.000E00").format(time));
				for (int n=0; n<cells.length; n++) {
					System.out.print("\t"+new DecimalFormat("0.000E00").format(cells[n].getQ()));
				}
				System.out.println();*/
				output(time,cells,ir,ic);
				if (time>parms.getMaxtime()) break;
			}
			vfile.close();
			rfile.close();
			ifile.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	void calcQV(){
		clear(a,b);
		
		for (int n=0; n<cells.length; n++) {
			cells[n].set_coefficients(a,b);
		}
//		test(a,b);
		
//		disp_array(a,b,null);

		DecompositionSolver solver = new LUDecompositionImpl(a).getSolver();
		RealVector x=solver.solve(b);
	
//		disp_array(a,b,x);

		for (int n=0; n<cells.length; n++) cells[n].get_qv(x);
		
/*		System.out.println("\tn\tV0\tV\tQ0\tQ");
		for (int n=0; n<cells.length; n++) {
			System.out.println(n+"\t"+new DecimalFormat("0.000E00").format(cells[n].getV0())
					+"\t"+new DecimalFormat("0.000E00").format(cells[n].getV())
					+"\t"+new DecimalFormat("0.000E00").format(cells[n].getQ0())
					+"\t"+new DecimalFormat("0.000E00").format(cells[n].getQ()));
		}*/
	}
	void clear(RealMatrix a, RealVector b){
		int nc=a.getColumnDimension();
		int nr=a.getRowDimension();
		int n=b.getDimension();
		for (int i=0; i<nc; i++){
			for (int j=0; j<nr; j++){
				a.setEntry(i,j,0.0);
			}
		}
	}
	void test (RealMatrix a, RealVector b) {
		int nz=parms.getNz();
		int num=nz*2;
		RealVector x=new ArrayRealVector(num);
		for (int n=0; n<nz; n++) {
			x.setEntry(2*n, cells[n].getQ0());
			x.setEntry(2*n+1, cells[n].getV0());
		}
		RealVector y=a.operate(x);
		System.out.println("i\tb\ty");
		for (int i=0; i<2*nz; i++) {
			System.out.println(i
					+"\t"+new DecimalFormat("0.000E00").format(b.getEntry(i))
					+"\t"+new DecimalFormat("0.000E00").format(y.getEntry(i)));
		}
	}
	void disp_array(RealMatrix a, RealVector b, RealVector x){
		int nc=a.getColumnDimension();
		int nr=a.getRowDimension();
		for (int i=0; i<nc; i++){
			for (int j=0; j<nr; j++){
				System.out.print("\t"+new DecimalFormat("0.000E00").format(a.getEntry(i,j)));
			}
			System.out.print("\t"+new DecimalFormat("0.000E00").format(b.getEntry(i)));
			if (x!=null) System.out.print("\t"+new DecimalFormat("0.000E00").format(x.getEntry(i)));
			System.out.println();
			
		}
		System.out.println();
	}
	void output(double time, T1dcell[] cells, double ir, double ic){
		if (time==0.0) {
			pQ = new double[cells.length];
			shift_Q();
		}
		vfile.print(new DecimalFormat("0.000E00").format(time)+
				"\t"+new DecimalFormat("0.000E00").format(ir)+
				"\t"+new DecimalFormat("0.000E00").format(ic));
		rfile.print(new DecimalFormat("0.000E00").format(time));
		ifile.print(new DecimalFormat("0.000E00").format(time));
		for (int n=0; n<cells.length; n++){
			vfile.print("\t"+new DecimalFormat("0.000E00").format(cells[n].getV()));
			rfile.print("\t"+new DecimalFormat("0.000E00").format(cells[n].getR()));
			
			double Vm = 0.0;
			if (n>0) Vm = cells[n-1].getV();
//			double current = (cells[n].getV() - Vm)/cells[n].getR() + ( cells[n].getQ() - pQ[n] )/dt;
			double current = (cells[n].getV() - Vm)/cells[n].getR();
			ifile.print("\t"+new DecimalFormat("0.000E00").format(current));
		}
		shift_Q();
		vfile.println();
		rfile.println();
		ifile.println();
	}
	void shift_Q() {
		for (int n=0; n<cells.length; n++) pQ[n]=cells[n].getQ();
	}
}
