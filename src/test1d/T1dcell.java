package test1d;

import org.apache.commons.math.linear.*;

public abstract class T1dcell {
	T1dcell cm,cp,cpp;
	double V,V0;
	double Q,Q0;
	double J,J0;
	double C;
	double R,R0;
	double dt;
	int i,i2;			//	location of the cell
	boolean on_f=false;	//	true if breakdown
	
	double Vglobal;		//	global voltage

	T1dcell(){}
	T1dcell(int i) { 
		this.i=i;
		i2=i*2;
	}
	abstract void set_adjacent(T1dcell cm, T1dcell cp);
	abstract void set_coefficients(RealMatrix a, RealVector b);
	abstract void get_qv(RealVector x);
	
	void set(double R, double C, double Q, double V) {
		set(R,C,Q,V,0.0);
	}
	void set(double R, double C, double Q, double V, double J){
		this.R=R;
		this.C=C;
		this.Q=Q;
		this.V=V;
		this.J=J;
	}
	void shift(){
		V0=V;
		Q0=Q;
		R0=R;
		J0=J;
	}
	void calc_Qeq(RealMatrix a, RealVector b) {
		a.setEntry(i2,i2-1,-1.0);
		a.setEntry(i2,i2  ,-1.0/C);
		a.setEntry(i2,i2+1,1.0);
		b.setEntry(i2,-cm.getV0()-Q0/C+V0);
	}
	void calc_Veq(RealMatrix a, RealVector b) {
		a.setEntry(i2+1,i2-1,0.5/R);
		a.setEntry(i2+1,i2  ,-1.0/dt);
		a.setEntry(i2+1,i2+1,-Rinv());
		a.setEntry(i2+1,i2+2,+1.0/dt);
		a.setEntry(i2+1,i2+3,0.5/cp.getR());
		b.setEntry(i2+1,-0.5*cm.getV0()/R0-Q0/dt+VRinv0()+cp.getQ0()/dt-0.5*cp.getV0()/cp.getR0()-Jav());
	}
	void get(RealVector x) {
		Q=x.getEntry(i2);
		V=x.getEntry(i2+1);
	}

	double Rinv(){ return 0.5*(1.0/R+1.0/cp.getR()); }
	double VRinv0(){ return 0.5*V0*(1.0/R0+1.0/cp.getR0()); }
	double Jav(){ return 0.5*(J+J0); }

	void setDt(double dt){ this.dt=dt; }
	void setR(double R){ this.R=R; }
	void setV(double V){ this.V=V; }
	void setQ(double Q){ this.Q=Q; }
	void setVglobal(double Vglobal) { this.Vglobal=Vglobal; }
	void setOn_f() { on_f=true; }
	void resetOn_f() { on_f=false; }
	
	int getI() { return i; }
	double getV(){ return V; }
	double getQ(){ return Q; }
	double getC(){ return C; }
	double getR(){ return R; }
	double getJ(){ return J; }
	double getV0(){ return V0; }
	double getQ0(){ return Q0; }
	double getR0(){ return R0; }
	double getJ0(){ return J0; }
	boolean IsOn() { return on_f; }
}
