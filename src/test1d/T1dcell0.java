package test1d;

import org.apache.commons.math.linear.*;

public class T1dcell0 extends T1dcell {
	T1dcell0(){}
	T1dcell0(int i){ super(i); }
	void set_adjacent(T1dcell cm, T1dcell cp) {
		this.cp=cp;
	}
	void set_coefficients(RealMatrix a, RealVector b) {
		a.setEntry(0,0,-1.0/C);
		a.setEntry(0,1,1.0);
		b.setEntry(0,-Q0/C+V0);		

		a.setEntry(i2+1,i2  ,-1.0/dt);
		a.setEntry(i2+1,i2+1,-Rinv());
		a.setEntry(i2+1,i2+2,1.0/dt);
		a.setEntry(i2+1,i2+3,0.5/cp.getR());
		b.setEntry(i2+1,-Q0/dt+VRinv0()+cp.getQ0()/dt-0.5*cp.getV0()/cp.getR0()-Jav());
	}
	void get_qv(RealVector x){
		get(x);
	}
}
