package test1d;

import org.apache.commons.math.linear.*;

public class T1dcelli extends T1dcell {
	T1dcelli(){}
	T1dcelli(int n){ super(n); }
	void set_adjacent(T1dcell cm, T1dcell cp) {
		this.cm=cm;
		this.cp=cp;
	}
	void set_coefficients(RealMatrix a, RealVector b) {
		calc_Qeq(a,b);
		calc_Veq(a,b);
	}
	void get_qv(RealVector x){
		get(x);
	}
}
