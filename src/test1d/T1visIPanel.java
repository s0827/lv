package test1d;

import java.awt.Color;
import java.awt.Graphics;

public class T1visIPanel extends T1visPanel {			//	ピーク値の強調表示
	T1visIPanel(String label){
		super(label);
	}
	public void paint(Graphics gc) {
		if (D==null) return;
		gc.setColor(Color.white);
		gc.fillRect(0,0,panelwidth, panelheight);
		double[][] y=D.max_inten();
		for (int i=0; i<y.length; i++) {
			for (int j=0; j<y[i].length; j++) {
				int iy0=panelheight-j*10;
				int iy1=iy0-10;
				Color c=get_color(y[i][j]);
				gc.setColor(c);
				gc.drawLine(i, iy0, i, iy1);
			}
		}
		draw_label(gc);
	}
	Color get_color(double d) {
		int x=(int)(255.0*d);
		x=limit(x);
		Color c=new Color(x,x,x);
		return c;
	}
}
