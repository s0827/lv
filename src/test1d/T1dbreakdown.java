package test1d;

import java.util.*;
import java.io.*;
import java.text.*;

public class T1dbreakdown {
	T1dcell[] cells;
	double prate,ref_efield,ef_mult;
	double[] relative_pres;
	double dz,dt;
	int nz;
	Random rnd;
	PrintWriter efile,pfile;
	
	T1dbreakdown(T1dcell[] cells, T1dparms parms){
		this.cells=cells;
		prate=parms.getPrate();
		ref_efield=parms.getRef_efield();
		ef_mult=parms.getEf_mult();
		relative_pres=parms.getRelative_pres();
		dz=parms.getDz();
		dt=parms.getTimestep();
		nz=parms.getNz();
		rnd=new Random(parms.getSeed());
		try {
			efile=new PrintWriter(new BufferedWriter(new FileWriter("efile.txt")));
			pfile=new PrintWriter(new BufferedWriter(new FileWriter("pfile.txt")));
			
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	T1dcell get_next_cell(double time) {
		T1dcell cell=null;
		double[] efield=calc_efield();
		double[] prob=new double[nz];	//	probability of ionization in dt
		double total_prob=0.0;
		for (int n=0; n<nz; n++) {
			double rate=prate*Math.pow(efield[n]/ref_efield,ef_mult);
			rate=rate/relative_pres[n];
			prob[n]=1.0-Math.exp(-rate*dt);
			total_prob+=prob[n];
		}
		if (total_prob>0.0) {
			if (total_prob>1.0) {			//	normalize to 1 if total_prob exceeds 1
				for (int n=0; n<nz; n++) prob[n]=prob[n]/total_prob;
			}
			TreeMap<Double, Integer> map=new TreeMap<Double, Integer>();
			double cuml_prob=0.0;
			for (int n=0; n<nz; n++) {
				if (!cells[n].IsOn()) {
					cuml_prob+=prob[n];
					map.put(cuml_prob, n);					
				}
			}
			if (cuml_prob>0.0) {
				Map.Entry<Double, Integer> Entry=map.ceilingEntry(rnd.nextDouble());
				
				if (Entry!=null) {
					Integer N=Entry.getValue();
//					if (N!=nz-1) {	//	keep state of last cell of (for debug)
						cell=cells[N.intValue()];
						cell.setOn_f();
//					}
				}				
			}
		}
		output(time,efield,prob,cell,cells);
		return cell;
	}
	double[] calc_efield() {
		double[] efield=new double[nz];
		for (int n= 0; n<nz; n++) {
			double v0=0.0;
			if (n>0) v0=cells[n-1].getV();
			double v1=cells[n].getV();
			efield[n]=(v1-v0)/dz;
		}
		return efield;
	}
	void output(double time, double[] efield, double[] prob, T1dcell cell, T1dcell[] cells) {
		efile.print(new DecimalFormat("0.000E00").format(time));
		pfile.print(new DecimalFormat("0.000E00").format(time));
		for (int n=0; n<efield.length; n++) {
			efile.print("\t"+new DecimalFormat("0.000E00").format(efield[n]));
			pfile.print("\t"+new DecimalFormat("0.00000").format(prob[n]));
		}
		int n_on=0;
		for (int n=0; n<cells.length; n++) {
			if (cells[n].IsOn()) n_on++;
		}
		Integer N=null;
		if (cell!=null) N=cell.getI();
		efile.println();
		String s0=new DecimalFormat("0").format(n_on);
		String s1="-";
		if (N!=null) s1=new DecimalFormat("0").format(N);
		pfile.println("\t"+s0+"\t"+s1);
	}
	void close() {
		efile.close();
		pfile.close();
	}
}
