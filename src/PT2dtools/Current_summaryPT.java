package PT2dtools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.*;

public class Current_summaryPT {					//	MXcurrent_summary2をPT2d用に改良
	String path,sys_id,did;
//	double oncurrent=1.0e+3;
	double cmax=0.1,pmax=0.5;	
	public static void main(String args[]){
		Current_summaryPT cs=new Current_summaryPT();
	}
	Current_summaryPT(){
		Defaultpath dp=new Defaultpath();
		path=dp.getPath();
		sys_id=dp.getSys_id();
		did=dp.getDid();
		BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
		LinkedHashMap<String, TreeMap<Double, CSd>> map=new LinkedHashMap<String, TreeMap<Double, CSd>>();
		try {
			System.out.print("number of cells " );
			String line=in.readLine();
			int numcell=Integer.parseInt(line);
			System.out.print("enter data id(PT-1-nnnn) ");
			line=in.readLine();
			StringTokenizer tokens=new StringTokenizer(line," \t");
			while(tokens.hasMoreTokens()){
				Integer num=Integer.parseInt(tokens.nextToken());
				String fnum=new DecimalFormat("0000").format(num);
				TreeMap<Double, CSd> data=read_summary_file(fnum,numcell);
				map.put(fnum,data);
			}
			System.out.print("cmax ");
			line=in.readLine();
			if (line!=null) cmax=Double.parseDouble(line);
			System.out.print("pmax ");
			line=in.readLine();
			if (line!=null) pmax=Double.parseDouble(line);
//			check(map);	
			output(map);
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	TreeMap<Double, CSd> read_summary_file(String fnum, int numcell){
		String e10_3="([\\-]{0,1}[0-9]\\.[0-9]{3}E[\\-]{0,1}[0-9]{2})";
		String inum="([0-9]+)";
		Pattern pat=Pattern.compile(e10_3+" "+inum+" "+inum+"  "+e10_3+" "+e10_3+" "+e10_3+" "+e10_3+" "+e10_3);
		TreeMap<Double, CSd> data=new TreeMap<Double, CSd>();
		try {
			String id=did+"-"+sys_id+"-"+fnum;
			BufferedReader in=new BufferedReader(new FileReader(path+id+"/"+id+"-summary.txt"));
			while(true){
				String line=in.readLine();
				if (line==null) break;
				Matcher mat=pat.matcher(line);
				if (mat.find()){
					double time=Double.parseDouble(mat.group(1));
					double current=Double.parseDouble(mat.group(5));
					double p=(double)Integer.parseInt(mat.group(3))/(double)numcell;
					data.put(new Double(time), new CSd(current,p));
				}
			}
			in.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
		return data;		
	}
	void output(LinkedHashMap<String, TreeMap<Double, CSd>> map){
		int size=map.size();
		Iterator<String> im=map.keySet().iterator();
		int num=0;
		while(im.hasNext()) num=Math.max(num,map.get(im.next()).size());
		String[] fnum=new String[size];
		double[] time=new double[num];
		double[][] d=new double[2*size][num];
//		System.out.println("Max current[A]="+new DecimalFormat("0.000E00").format(dmax));
		Iterator<String> is=map.keySet().iterator();
		int ic=0;
		while(is.hasNext()){
			fnum[ic]=is.next();
			TreeMap<Double, CSd> data=map.get(fnum[ic]);
			Iterator<Double> it=data.keySet().iterator();
			int i=0;
			while(it.hasNext()){
				Double TIME=it.next();
				if (ic==0){
					time[i]=TIME.doubleValue();
				}
				d[ic][i]=data.get(TIME).getCurrent()+cmax*(double)ic;
				d[size+ic][i]=data.get(TIME).getP()+pmax*(double)ic;
				i++;
			}
			ic++;
		}
		try {
			String filename=did+"-"+sys_id+"-"+fnum[0]+"-"+fnum[size-1]+"-current_summary.txt";
			PrintWriter out=new PrintWriter(new BufferedWriter(new FileWriter(filename)));
			out.print("time[s]");
			for (ic=0; ic<size; ic++) out.print("\t"+fnum[ic]+"c");
			for (ic=0; ic<size; ic++) out.print("\t"+fnum[ic]+"p");
			out.println();
			for (int i=0; i<num; i++){
				out.print(new DecimalFormat("0.000E00").format(time[i]));
				for (ic=0; ic<size; ic++) out.print("\t"+new DecimalFormat("0.00000").format(d[ic][i]));
				for (ic=0; ic<size; ic++) out.print("\t"+new DecimalFormat("0.00000").format(d[size+ic][i]));
				out.println();
			}
			out.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
}
class CSd{
	double current,p;
	CSd(double current, double p){
		this.current=current;
		this.p=p;
	}
	double getCurrent(){ return current; }
	double getP(){ return p; }
}
