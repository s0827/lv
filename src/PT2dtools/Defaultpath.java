package PT2dtools;

import java.util.*;
import java.io.*;

public class Defaultpath {
	String path;
	String sys_id;
	String did;
	Defaultpath(){
		try {
			BufferedReader in=new BufferedReader(new FileReader("defaultpath.txt"));
			while(true){
				String line=in.readLine();
				if (line==null) break;
				StringTokenizer tokens=new StringTokenizer(line," \t");
				if (tokens.hasMoreTokens()){
					String key=tokens.nextToken();
					if (tokens.hasMoreTokens()){
						String value=tokens.nextToken();
						if (key.equals("path")){
							path=value;
						}
						else if (key.equals("sys_id")){
							sys_id=value;
						}
						else if (key.equals("did")){
							did=value;
						}
					}
				}
			}
			in.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	String getPath(){ return path; }
	String getSys_id(){ return sys_id; }
	String getDid(){ return did; }
}
