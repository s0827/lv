package PT2dtools;

import java.io.*;
import java.util.*;
import java.util.regex.*;
import java.text.*;
import PT2d.*;

public class PT2dleader {				//	リーダーの成長を解析する。
	String path,sys_id,did;
	int sx=12,sy=24;
	String e10_3="([\\-]{0,1}[0-9]\\.[0-9]{3}E[\\-]{0,1}[0-9]{2})";
	String inum="([0-9]+)";
	String rnum="([0-9]\\.[0-9]+)";
	int minlength=3;					//	最小リーダー長さ（針電極の長さをハードコード）
	int offset=25;
	public static void main(String args[]){
		PT2dleader pl=new PT2dleader();
	}
	PT2dleader(){
		Defaultpath dp=new Defaultpath();
		path=dp.getPath();
		sys_id=dp.getSys_id();
		did=dp.getDid();
		System.out.println("Stem = ["+sx+","+sy+"]");
		BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
		LinkedHashMap<String, PT2dleaderd[]> data=new LinkedHashMap<String, PT2dleaderd[]>();
		try {
			System.out.print("enter data id (nnnn) ");
			String line=in.readLine();
			StringTokenizer tokens=new StringTokenizer(line," \t");
			int i=0;
			while(tokens.hasMoreTokens()){
				String fnum=tokens.nextToken();
				PT2dleaderd[] d=read_summary_file(fnum);
				read_cluster_file(i,fnum,d);
				data.put(fnum,d);
				i++;
			}
			output(data);
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	PT2dleaderd[] read_summary_file(String fnum){
		Pattern pat=Pattern.compile(e10_3+" "+inum+" "+inum+"  "+e10_3+" "+e10_3+" "+e10_3+" "+e10_3+" "+e10_3);
		int maxsteps=0;
		TreeMap<Integer, PT2dleaderd> data=new TreeMap<Integer, PT2dleaderd>();
		try {
			String id=did+"-"+sys_id+"-"+fnum;
			BufferedReader in=new BufferedReader(new FileReader(path+id+"/"+id+"-summary.txt"));
			while(true){
				String line=in.readLine();
				if (line==null) break;
				Matcher mat=pat.matcher(line);
				if (mat.find()){
					double time=Double.parseDouble(mat.group(1));
					int step=Integer.parseInt(mat.group(2));
					int numcell=Integer.parseInt(mat.group(3));
					double current=Double.parseDouble(mat.group(5));
					maxsteps=Math.max(step,maxsteps);
					PT2dleaderd d=new PT2dleaderd();
					d.put_summary(step,time,current,numcell);
					data.put(step,d);
				}
			}
			in.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
		return get_summary(data,maxsteps);
	}
	PT2dleaderd[] get_summary(TreeMap<Integer, PT2dleaderd> data, int maxsteps){
		PT2dleaderd[] d=new PT2dleaderd[maxsteps];
		Iterator<Integer> it=data.keySet().iterator();
		while(it.hasNext()){
			Integer I=it.next();
			d[I.intValue()-1]=data.get(I);
		}
		return d;
	}
	void read_cluster_file(int i, String fnum, PT2dleaderd[] d){
		Pattern pat=Pattern.compile("step ("+inum+") numcell ("+inum+")");
		PT2dclusterparser parser=new PT2dclusterparser();
		try {
			String id=did+"-"+sys_id+"-"+fnum;
			BufferedReader in=new BufferedReader(new FileReader(path+id+"/"+id+"-cluster.txt"));
			while(true){
				String line=in.readLine();
				if (line==null) break;
				Matcher mat=pat.matcher(line);
				if (mat.find()){
					int step=Integer.parseInt(mat.group(1));
					LinkedList<Cell> clusters=parser.read_cluster(in);
					analyze_clusters(i,step,clusters,d[step]);
				}
			}
			filldata(i,d);		//	データの隙間部分を埋める。
			in.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	void analyze_clusters(int i, int step, LinkedList<Cell> clusters, PT2dleaderd d){
		Cell lcell=find_leader(clusters);
//		System.out.print("step "+step+" num_clusters "+clusters.size());
		if (lcell!=null){
			int miny=get_miny(lcell);
			int leader_length=sy-miny+1;
//			System.out.print(" "+lcell.get_cell_set().size()+" leader_length "+leader_length);
			d.setLeader_length(leader_length+i*offset);
		}
//		System.out.println(" ");
	}
	void filldata(int i,PT2dleaderd[] d){
		int currentlength=minlength+i*offset;
		for (PT2dleaderd dd: d){
			if (dd.getLeader_length()==-1) {
				dd.setLeader_length(currentlength);
			}
			else {
				currentlength=dd.getLeader_length();
			}
		}
	}
	Cell find_leader(LinkedList<Cell> clusters){
		Cell lcell=null;
		for (Cell hcell: clusters){
			LinkedList<Cell> set=hcell.get_cell_set();
			for (Cell cell: set){
//				System.out.print(" "+cell.getId());
				int ix=cell.getIx();
				int iy=cell.getIy();
//				System.out.println(ix+" "+iy);
				if (ix==sx && iy==sy) {
					lcell=hcell;
					break;
				}
			}
//			System.out.println();
			if (lcell!=null) break;
		}
		return lcell;
	}
	int get_miny(Cell lcell){
		int miny=sy;
		LinkedList<Cell> set=lcell.get_cell_set();
		for (Cell cell: set) miny=Math.min(miny,cell.getIy());
//		System.out.println(" "+miny);
		return miny;
	}
	void output(LinkedHashMap<String, PT2dleaderd[]> data){
		String filename=get_filename(data);						//	出力ファイル名生成
		LinkedHashMap<String, Double> tmap=get_timemap(data);
		try {
			PrintWriter out=new PrintWriter(new BufferedWriter(new FileWriter(filename)));
			out.print("time [s]");
			Iterator<String> it=data.keySet().iterator();
			while(it.hasNext()) {
				String f=it.next();
				out.print("\t"+f+"l\t"+f+"c");
			}
			out.println();
			Iterator is=tmap.keySet().iterator();
			int i=0;
			while(is.hasNext()){
				double time=tmap.get(is.next()).doubleValue();
				out.print(new DecimalFormat("0.000E00").format(time));
				it=data.keySet().iterator();
				while(it.hasNext()){
					PT2dleaderd[] d=data.get(it.next());
					out.print("\t"+new DecimalFormat("0").format(d[i].getLeader_length()));					
					out.print("\t"+new DecimalFormat("0.000E00").format(d[i].getCurrent()));					
				}
				out.println();
				i++;
			}
			out.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
	
	}
	String get_filename(LinkedHashMap<String, PT2dleaderd[]> data){
		Iterator<String> it=data.keySet().iterator();
		String first="-"+it.next();
		String last="";
		while(it.hasNext()) last="-"+it.next();
		String filename=did+"-"+sys_id+first+last+"-leader.txt";
		return filename;		
	}
	LinkedHashMap<String, Double> get_timemap(LinkedHashMap<String, PT2dleaderd[]> data){
		LinkedHashMap<String, Double> tmap=new LinkedHashMap<String, Double>();
		Iterator<String> it=data.keySet().iterator();
		while(it.hasNext()){
			PT2dleaderd[] d=data.get(it.next());
			for (PT2dleaderd dd: d){
				int step=dd.getStep();
				String S=new DecimalFormat("0").format(step);
				Double T=tmap.get(S);
				if (T==null){
					tmap.put(S,new Double(dd.getTime()));
				}
				else {
					double time=T.doubleValue();
					if (time!=0.0){
						double diff=Math.abs(time-dd.getTime())/(time+dd.getTime());
						if (diff>1.0e-5){
							System.out.println("データの時間刻みが不一致");
							System.exit(0);
						}
					}
				}
			}
		}
		return tmap;
	}
}
