package PT2dtools;

import java.io.*;
import java.util.*;
import java.util.regex.*;

public class Trace_cell {
	String dir="/Users/sasaki/exec/PT2dSF6/130522/";
	String did="0";
	String num="[0-9]+\\.[0-9]+";
	public static void main(String args[]){
		Trace_cell tc=new Trace_cell();		
	}
	Trace_cell(){
		try {
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			System.out.print("Enter data id ");
			String fnum=in.readLine();
			System.out.print("Enter cell id ");
			String cellid=in.readLine();
			
			String id="PT-"+did+"-"+fnum;
			String filename=dir+id+"/"+id+"-cluster.txt";
			trace(filename,cellid);
			
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	void trace(String filename, String cellid){
		Pattern cpat=Pattern.compile("step ([0-9]+) create p="+num+" remove p="+num);
		try {
			BufferedReader in=new BufferedReader(new FileReader(filename));
			while(true){
				String line=in.readLine();
				if (line==null) break;
				Matcher cmat=cpat.matcher(line);
				if (cmat.find()) {
					int i=Integer.parseInt(cmat.group(1));
					System.out.print("step "+i);
					crinfo(in,cellid);
					clusterinfo(in,cellid);
					System.out.println();
				}
			}
			in.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	void crinfo(BufferedReader in, String cellid){
		Pattern crpat=Pattern.compile("\\(([CR]) [0-9]+ ([0-9 ]+)\\)");
		Pattern cpat=Pattern.compile("[0-9]{6}");
		try {
			while(true){
				String line=in.readLine();
				Matcher crmat=crpat.matcher(line);
				if (crmat.find()){
					String cells=crmat.group(2);
					Matcher cmat=cpat.matcher(cells);
					boolean fcell=false;
					while(cmat.find()){
						if (cmat.group(0).equals(cellid)){
							fcell=true;
							break;
						}
					}
					if (fcell){
						String s=crmat.group(1);
						if (s.equals("C")) {
							System.out.print(" create");
						}
						else {
							System.out.print(" remove");
						}
					}
				}
				else {
					break;
				}
			}
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	void clusterinfo(BufferedReader in, String cellid){
		TreeMap<String, FDcount> map=analyze_clusters(in);
		FDcount C=map.get(cellid);
		if (C!=null){
			System.out.print(" exists ("+C.getCount()+")");
		}
	}
	TreeMap<String, FDcount> analyze_clusters(BufferedReader in){
		TreeMap<String, FDcount> map=new TreeMap<String, FDcount>();
		String rnum="[0-9]+\\.[0-9]+";
		Pattern epat=Pattern.compile("numclusters [0-9]+ numcell [0-9]+ maxsize [0-9]+ avrsize "+rnum);
		Pattern cpat=Pattern.compile("[0-9]{6}");
		try {
			while(true){
				String line=in.readLine();
				Matcher emat=epat.matcher(line);
				if (emat.find()) break;
				Matcher cmat=cpat.matcher(line);
				while(cmat.find()){
					String key=cmat.group(0);
					FDcount C=map.get(key);
					if (C==null){
						map.put(key,new FDcount());
					}
					else {
						C.incr();
					}
				}
			}			
		}
		catch(Exception e){
			System.err.println(e);
		}
		return map;
	}
}
