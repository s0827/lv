package PT2dtools;

import java.io.*;
import java.util.*;
import java.util.regex.*;
import java.text.*;

import PT2d.*;

public class PT2dlist {			//	データを計算条件をまとめてリスト表示する。
	String path="/users/sasaki/exec/PT2dSF6/";
	String ddir="data-ichida/";
	public static void main(String args[]){
		PT2dlist pl=new PT2dlist();
	}
	PT2dlist(){
		Pattern pat=Pattern.compile("PT\\-([0-9]+)\\-([0-9]{4})");
		TreeSet<String> keys=new TreeSet<String>();
		File mdir=new File(path+ddir);
		File[] dirs=mdir.listFiles();
		String hid="10";
		for (int i=0; i<dirs.length; i++){
			String dirname=dirs[i].getName();
			Matcher mat=pat.matcher(dirname);
			if (mat.find()) {
				hid=mat.group(1);
				keys.add(mat.group(2));	
			}
		}
		show_file_list(hid,keys);
	}
	void show_file_list(String hid, TreeSet<String> keys){
		int i=0;
		try {
			String outfile=path+"filelist.txt";
			PrintWriter out=new PrintWriter(new BufferedWriter(new FileWriter(outfile)));
			System.out.println("flags : E efield effect of ionization rate");
			System.out.println("        N no recombination, C constant, J current effect");
			for (String key: keys){
				String id="PT-"+hid+"-"+key;
				String xmlfile=path+ddir+id+"/"+id+"-input_PT2d.xml";
				PT2dparms parms=new PT2dparms(xmlfile,true);
				if (i%10==0) {
					System.out.println("-------------------------------------------------------------------------------------------------------------------------------");
					out.println("-------------------------------------------------------------------------------------------------------------------------------");
				}
				if (i%20==0) {
					System.out.println("     id    rx    ry   nx   ny     r   eps  maxtime timestep   seed       Vp        e0    rate0     mult    efmin factr       j0");
					out.println("     id    rx    ry   nx   ny     r   eps  maxtime timestep   seed       Vp        e0    rate0     mult    efmin factr       j0");
				}
				String istr=".";
				if (parms.getEf_f()) istr="E";
				String rstr="N";
				if (parms.getRecomb_f()){
					if (parms.getCr_f()){
						rstr="J";
					}
					else {
						rstr="C";
					}
				}
				System.out.println(
						fill(hid,2)+" "+key+" "+formatf(5,2,parms.getRx())+" "+formatf(5,2,parms.getRx())
						+" "+formati(4,parms.getNx())+" "+formati(4,parms.getNy())
						+" "+formatf(5,1,parms.getR())+" "+formatf(5,1,parms.getEps())
						+" "+formate(8,1,parms.getMaxtime())+" "+formate(8,1,parms.getTimestep())
						+" "+istr+rstr+" "+formati(3,parms.getSEED())+" "+formate(8,1,parms.getVp())
						+" "+formate(9,2,parms.getE0())+" "+formate(8,1,parms.getRate0())+" "+formate(8,1,parms.getMult())
						+" "+formate(8,1,parms.getEfmin())+" "+formatf(5,2,parms.getR_factor())+" "+formate(8,1,parms.getJ0())
						);
				out.println(
						fill(hid,2)+" "+key+" "+formatf(5,2,parms.getRx())+" "+formatf(5,2,parms.getRx())
						+" "+formati(4,parms.getNx())+" "+formati(4,parms.getNy())
						+" "+formatf(5,1,parms.getR())+" "+formatf(5,1,parms.getEps())
						+" "+formate(8,1,parms.getMaxtime())+" "+formate(8,1,parms.getTimestep())
						+" "+istr+rstr+" "+formati(3,parms.getSEED())+" "+formate(8,1,parms.getVp())
						+" "+formate(9,2,parms.getE0())+" "+formate(8,1,parms.getRate0())+" "+formate(8,1,parms.getMult())
						+" "+formate(8,1,parms.getEfmin())+" "+formatf(5,2,parms.getR_factor())+" "+formate(8,1,parms.getJ0())
						);
				i++;
			}
			out.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	
	String formati(int length, int num){
		String str=new DecimalFormat("0").format(num);
		return fill(str,length);
	}
	String formatf(int length0, int length1, double f){
		String str=new DecimalFormat(formstr(length1)).format(f);
		return fill(str,length0);
	}
	String formate(int length0, int length1, double f){
		String str=new DecimalFormat(formstr(length1)+"E00").format(f);
		return fill(str,length0);
	}
	String formstr(int length){
		String str="0";
		if (length>0) str+=".";
		for (int i=0; i<length; i++) str+="0";		
		return str;
	}
	String fill(String instr, int length){
		String outstr=instr;
		while(outstr.length()<length) {
			outstr=" "+outstr;
		}
		return outstr;
	}
}
