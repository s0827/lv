package PT2dtools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Compare_current {
	String path,sys_id,did;
	double offset;
	public static void main(String args[]){
		Compare_current cc=new Compare_current();
	}
	Compare_current(){
		Defaultpath dp=new Defaultpath();
		path=dp.getPath();
		sys_id=dp.getSys_id();
		did=dp.getDid();
		BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
		LinkedHashMap<String, TreeMap<Double, Double>> map=new LinkedHashMap<String, TreeMap<Double, Double>>();
		try {
			System.out.print("enter data id(PT-1-nnnn) ");
			String line=in.readLine();
			StringTokenizer tokens=new StringTokenizer(line," \t");
			while(tokens.hasMoreTokens()){
				Integer num=Integer.parseInt(tokens.nextToken());
				String fnum=new DecimalFormat("0000").format(num);
				TreeMap<Double, Double> data=read_current(fnum);
				map.put(fnum,data);
			}
			System.out.print("offset ");
			offset=Double.parseDouble(in.readLine());
//			check(map);	
			output(map);
		}
		catch(Exception e){
			System.err.println(e);
		}
	}		
	TreeMap<Double, Double> read_current(String fnum){
		String e10_3="([\\-]{0,1}[0-9]\\.[0-9]{3}E[\\-]{0,1}[0-9]{2})";
		String inum="([0-9]+)";
		Pattern pat=Pattern.compile(e10_3+" "+inum+" "+inum+"  "+e10_3+" "+e10_3+" "+e10_3+" "+e10_3+" "+e10_3);
		TreeMap<Double, Double> data=new TreeMap<Double, Double>();
		try {
			String id=did+"-"+sys_id+"-"+fnum;
			BufferedReader in=new BufferedReader(new FileReader(path+id+"/"+id+"-summary.txt"));
			while(true){
				String line=in.readLine();
				if (line==null) break;
				Matcher mat=pat.matcher(line);
				if (mat.find()){
					double time=Double.parseDouble(mat.group(1));
					double current=Double.parseDouble(mat.group(5));
					data.put(new Double(time), new Double(current));
				}
			}
			in.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
		return data;		
	}
	
	void output(LinkedHashMap<String, TreeMap<Double, Double>> map){
		int size=map.size();
		Iterator<String> im=map.keySet().iterator();
		int num=0;
		while(im.hasNext()) num=Math.max(num,map.get(im.next()).size());
		String[] fnum=new String[size];
		double[] time=new double[num];
		double[][] d=new double[size][num];
		Iterator<String> is=map.keySet().iterator();
		int ic=0;
		while(is.hasNext()){
			fnum[ic]=is.next();
			TreeMap<Double, Double> data=map.get(fnum[ic]);
			Iterator<Double> it=data.keySet().iterator();
			int i=0;
			while(it.hasNext()){
				Double TIME=it.next();
				if (ic==0){
					time[i]=TIME.doubleValue();
				}
				d[ic][i]=data.get(TIME).doubleValue()+offset*(double)ic;
				i++;
			}
			ic++;
		}
		try {
			String filename=did+"-"+sys_id+"-"+fnum[0]+"-"+fnum[size-1]+"-compare_current.txt";
			PrintWriter out=new PrintWriter(new BufferedWriter(new FileWriter(filename)));
			out.print("time[s]");
			for (ic=0; ic<size; ic++) out.print("\t"+fnum[ic]+"c");
			out.println();
			for (int i=0; i<num; i++){
				out.print(new DecimalFormat("0.000E00").format(time[i]));
				for (ic=0; ic<size; ic++) out.print("\t"+new DecimalFormat("0.00000").format(d[ic][i]));
				out.println();
			}
			out.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
	}	
}
