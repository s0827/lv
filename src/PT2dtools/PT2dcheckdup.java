package PT2dtools;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import PT2d.Cell;

public class PT2dcheckdup {		//	セルの重複生成をチェックする。
	String path,sys_id,did;
	String inum="[0-9]+";
	String rnum="[0-9]+\\.[0-9]+";
	public static void main(String args[]){
		PT2dcheckdup pd=new PT2dcheckdup();
	}
	PT2dcheckdup(){
		Defaultpath dp=new Defaultpath();
		path=dp.getPath();
		sys_id=dp.getSys_id();
		did=dp.getDid();
		try {
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			System.out.print("enter data id ");
			String fnum=in.readLine();
			check(fnum);
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	void check(String fnum){
		Pattern pat=Pattern.compile("step ("+inum+") numcell ("+inum+")");
		try {
			String id=did+"-"+sys_id+"-"+fnum;
			BufferedReader in=new BufferedReader(new FileReader(path+id+"/"+id+"-cluster.txt"));
			while(true){
				String line=in.readLine();
				if (line==null) break;
				Matcher mat=pat.matcher(line);
				if (mat.find()){
					int step=Integer.parseInt(mat.group(1));
					boolean stat=check_cluster(in);
					if (stat) System.out.println(" at step "+step);
				}
			}
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	boolean check_cluster(BufferedReader in){
		Pattern dpat=Pattern.compile("("+inum+")>  (.*) \\("+inum+"\\)");
		Pattern epat=Pattern.compile("numclusters "+inum+" numcell "+inum+" maxsize "+inum
				+" avrsize "+rnum);
		boolean cstat=false;
		boolean stat=false;
		try {
			while(true){
				String line=in.readLine();
				Matcher emat=epat.matcher(line);
				if (emat.find()) break;
				Matcher dmat=dpat.matcher(line);
				if (dmat.find()) {
					cstat=analyze_cluster(dmat.group(2));
					if (cstat) {
						System.out.print (" cluster "+dmat.group(1));
						stat=true;
					}
				}
			}			
		}
		catch(Exception e){
			System.err.println(e);
		}
//		System.out.println(stat);
		return stat;
	}
	boolean analyze_cluster(String line){
		Pattern pat=Pattern.compile("([0-9]{2}[0-9]{2}[0-9]{2})");
		Matcher mat=pat.matcher(line);
		TreeSet<String> set=new TreeSet<String>();
		boolean stat=false;
		while(mat.find()){
			String key=mat.group(1);
			if (set.contains(key)) {
				stat=true;
				System.out.print(" "+key);
			}
			else {
				set.add(key);
			}
		}
		return stat;
	}
}
