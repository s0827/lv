package PT2dtools;

public class PT2dleaderd {
	double time,current;
	int step,numcell;
	int leader_length=-1;
	PT2dleaderd(){}
	void put_summary(int step, double time, double current, int numcell){
		this.step=step;
		this.time=time;
		this.current=current;
		this.numcell=numcell;
	}
	void setLeader_length(int leader_length){ this.leader_length=leader_length; }
	int getStep(){ return step; }
	double getTime(){ return time; }
	double getCurrent(){ return current; }
	int getNumcell(){ return numcell; }
	int getLeader_length(){ return leader_length; }
}
