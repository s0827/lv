package PT2dtools;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TurnonPT {			// turn on timeを検出する。簡易的な実装とする。
	String path,sys_id,did;
	double oncurrent=0.08;
	public static void main(String args[]){
		TurnonPT pt=new TurnonPT();
	}
	TurnonPT(){
		Defaultpath dp=new Defaultpath();
		path=dp.getPath();
		sys_id=dp.getSys_id();
		did=dp.getDid();
		BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
		LinkedHashMap<String, TreeMap<Double, CSd>> map=new LinkedHashMap<String, TreeMap<Double, CSd>>();
		try {
			System.out.print("enter data id(PT-1-nnnn) ");
			String line=in.readLine();
			StringTokenizer tokens=new StringTokenizer(line," \t");
			while(tokens.hasMoreTokens()){
				Integer num=Integer.parseInt(tokens.nextToken());
				String fnum=new DecimalFormat("0000").format(num);
				analyze_summary_file(fnum);
			}
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	void analyze_summary_file(String fnum){
		String e10_3="([\\-]{0,1}[0-9]\\.[0-9]{3}E[\\-]{0,1}[0-9]{2})";
		String inum="([0-9]+)";
		Pattern pat=Pattern.compile(e10_3+" "+inum+" "+inum+"  "+e10_3+" "+e10_3+" "+e10_3+" "+e10_3+" "+e10_3);
		System.out.print(fnum+"  ontime[s]= ");
		try {
			String id=did+"-"+sys_id+"-"+fnum;
			BufferedReader in=new BufferedReader(new FileReader(path+id+"/"+id+"-summary.txt"));
			while(true){
				String line=in.readLine();
				if (line==null) break;
				Matcher mat=pat.matcher(line);
				if (mat.find()){
					double time=Double.parseDouble(mat.group(1));
					double current=Double.parseDouble(mat.group(5));
					if (current>oncurrent) {
						System.out.print(new DecimalFormat("0.000E00").format(time));
						break;
					}
				}
			}
			in.close();
			System.out.println();
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
}
