package PT2dtools;

import java.text.*;
import java.io.*;

public class filetool {							//	ディレクトリの中のファイルの特定のもの以外を消す
	public static void main(String args[]){
		filetool ft=new filetool();
	}
	filetool(){
		String dir="/Users/sasaki/Desktop/percolation1207/data/";
		String pfx="PT-1-0869";
		String[] sfx={"c","d","g"};
		int fnum=1000;
		int iselect=200;
		try {
			Runtime rt=Runtime.getRuntime();
			for (int is=0; is<sfx.length; is++){
				for (int i=0; i<1000; i++){
					String filename=dir+pfx+"/"+pfx+"-"+sfx[is]+"-"+new DecimalFormat("000").format(i);
					if (sfx[is].equals("g")){
						filename+=".jpg";
					}
					else {
						filename+=".txt";
					}
					File file=new File(filename);
					if (file.exists()){
						if (i%iselect!=0){
							rt.exec("rm "+filename);
						}
						else {
							System.out.println(filename);
						}					
					}
				}						
			}
		}
		catch(Exception e){
			System.err.println(e);
		}

	}
}
