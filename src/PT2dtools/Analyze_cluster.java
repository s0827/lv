package PT2dtools;

import java.io.*;
import java.util.*;
import java.util.regex.*;

public class Analyze_cluster {
	String dir="/Users/sasaki/exec/PT2dSF6/data/";
	String did="1";
	String num="[0-9]+\\.[0-9]+";
	public static void main(String args[]){
		Analyze_cluster ac=new Analyze_cluster();
	}
	Analyze_cluster(){
		Pattern spat=Pattern.compile("step [0-9]+ create p="+num+" remove p="+num);
		Pattern hpat=Pattern.compile("step ([0-9]+) numcell [0-9]+");
		Pattern cpat=Pattern.compile("numclusters ([0-9]+) numcell ([0-9]+) maxsize ([0-9]+) avrsize ([0-9]+\\.[0-9]+)");
		try {
			BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
			System.out.print("Enter data id ");
			String fnum=br.readLine();
			String id="PT-"+did+"-"+fnum;
			
			String filename=dir+id+"/"+id+"-cluster.txt";
			System.out.println("step\tnumcell\tnumclusters\tmaxsize\tavrsize");
			BufferedReader in=new BufferedReader(new FileReader(filename));
			while(true){
				String line=in.readLine();
				if (line==null) break;
//				Matcher smat=spat.matcher(line);
//				if (smat.find()) System.out.println(smat.group(0));
				Matcher hmat=hpat.matcher(line);
				if (hmat.find()){
					int step=Integer.parseInt(hmat.group(1));
					while(true){
						line=in.readLine();
						Matcher cmat=cpat.matcher(line);
						if (cmat.find()){
							int numclusters=Integer.parseInt(cmat.group(1));
							int numcell=Integer.parseInt(cmat.group(2));
							int maxsize=Integer.parseInt(cmat.group(3));
							double avrsize=Double.parseDouble(cmat.group(4));
							System.out.println(step+"\t"+numcell+"\t"+numclusters+"\t"+maxsize+"\t"+avrsize);
							break;
						}
					}
				}
			}
			in.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
}
