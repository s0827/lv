package PT2dtools;

import PT2d.*;

import java.io.*;
import java.util.*;
import java.util.regex.*;

public class PT2dclusterparser {
	String inum="([0-9]+)";
	String rnum="([0-9]+\\.[0-9]+)";
	LinkedList<Cell> stack=new LinkedList<Cell>();
	public static void main(String args[]){
		PT2dclusterparser pp=new PT2dclusterparser();
//		pp.test();
		pp.test_file();
	}
	PT2dclusterparser(){		
	}
	Cell parse(Cell cell, StringTokenizer tokens){
		Pattern pat=Pattern.compile("([0-9]{2})([0-9]{2})([0-9]{2})");
		if (!tokens.hasMoreTokens()) return cell;
		String word=tokens.nextToken();
//		System.out.println(word);
		Matcher mat=pat.matcher(word);
		if (mat.find()){
			int ix=Integer.parseInt(mat.group(1));
			int iy=Integer.parseInt(mat.group(2));
			int iz=Integer.parseInt(mat.group(3));
			if (cell==null){
				cell=new Cell(ix,iy,iz);
				cell=parse(cell,tokens);
			}
			else {
				Cell child=new Cell(ix,iy,iz);
				child.setParent(cell);
				cell.add_child(child);
				child=parse(child,tokens);
			}
		}
		else {
			if (word.equals("(")){
				stack.push(cell);
				cell=parse(cell,tokens);
			}
			else if (word.equals("|")){
				cell=stack.pop();
				stack.push(cell);
				cell=parse(cell,tokens);
			}
			else if (word.equals(")")){
				cell=stack.pop();
				cell=parse(cell,tokens);
			}
		}
		return cell;
	}

	void test(){
		Pattern pat=Pattern.compile("[0-9]+>  (.*) \\([0-9]+\\)");
		try {
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			while(true){
				System.out.print("input:  ");
				String line=in.readLine();
				if (line==null) break;
				if (line.length()==0) break;
				Matcher mat=pat.matcher(line);
				if (mat.find()){
					StringTokenizer tokens=new StringTokenizer(mat.group(1)," ");
					Cell cell=parse(null,tokens);
					System.out.print("output: ");
					cell.output_cluster(null,true);
					System.out.println();
					System.out.println();
				}
			}
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	void test_file(){
		Pattern pat=Pattern.compile("step ("+inum+") numcell ("+inum+")");
		try {
			BufferedReader in=new BufferedReader(new FileReader("/Users/sasaki/exec/PT2d/1210data/PT-1-0188/PT-1-0188-cluster.txt"));
			while(true){
				String line=in.readLine();
				if (line==null) break;
				Matcher mat=pat.matcher(line);
				if (mat.find()){
					int step=Integer.parseInt(mat.group(1));
					System.out.println("step= "+step);
					read_cluster(in);				
				}
			}
			in.close();			
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	LinkedList<Cell> read_cluster(BufferedReader in){
		Pattern dpat=Pattern.compile(inum+">  (.*) \\("+inum+"\\)");
		Pattern epat=Pattern.compile("numclusters "+inum+" numcell "+inum+" maxsize "+inum+" avrsize "+rnum);
		LinkedList<Cell> clusters=new LinkedList<Cell>();
		try {
			while(true){
				String line=in.readLine();
//				System.out.println(line);
				Matcher emat=epat.matcher(line);
				if (emat.find()) break;
				Matcher dmat=dpat.matcher(line);
				if (dmat.find()) {
					String instr=dmat.group(2);
					StringTokenizer tokens=new StringTokenizer(instr," ");
					Cell cell=parse(null,tokens);
					instr=" "+instr;
					String outstr=cell.output_cluster();
					if (!instr.equals(outstr)){
						System.out.println(instr);
						System.out.println(outstr);
						System.out.println();
					}
					clusters.add(cell);
				}
			}			
		}
		catch(Exception e){
			System.err.println(e);
		}
		return clusters;
	}
}
