package PT2dtools;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.text.*;

public class FindPeek {
	String path,sys_id,did;
	int width=21;
	public static void main(String args[]){
		FindPeek fp=new FindPeek();
	}
	FindPeek(){
		Defaultpath dp=new Defaultpath();
		path=dp.getPath();
		sys_id=dp.getSys_id();
		did=dp.getDid();
		boolean minimum_f=false;
		try {
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			System.out.print("enter data id(PT-1-nnnn) ");
			String fnum=in.readLine();
			System.out.print("enter width ([-]nn) ( default "+width+", - to minimum) ");
			String cwidth=in.readLine();
			if (cwidth.length()>0) {
				Pattern pat=Pattern.compile("([\\-]*)([0-9]*)");
				Matcher mat=pat.matcher(cwidth);
				if (mat.find()){
					if (mat.group(1).equals("-")) minimum_f=true;
					if (mat.group(2).length()>0){
						width=Integer.parseInt(mat.group(2));						
					}
				}
			}
			read_file(fnum,width,minimum_f);		
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	void read_file(String fnum,int width,boolean minimum_f){
		String e10_3="([\\-]{0,1}[0-9]\\.[0-9]{3}E[\\-]{0,1}[0-9]{2})";
		String inum="([0-9]+)";
		Pattern pat=Pattern.compile(e10_3+" "+inum+" "+inum+"  "+e10_3+" "+e10_3+" "+e10_3+" "+e10_3+" "+e10_3);
		int[] count=new int[width];
		double[] current=new double[width];
		double[] time=new double[width];
		for (int i=0; i<current.length; i++) {
			count[i]=0;
			time[i]=0.0;
			current[i]=0.0;
		}
		try {
			String id=did+"-"+sys_id+"-"+fnum;
			BufferedReader in=new BufferedReader(new FileReader(path+id+"/"+id+"-summary.txt"));
			while(true){
				for (int i=0; i<current.length-1; i++) {
					count[i]=count[i+1];
					time[i]=time[i+1];
					current[i]=current[i+1];
				}
				String line=in.readLine();
				if (line==null) break;
				Matcher mat=pat.matcher(line);
				if (mat.find()){
					time[current.length-1]=Double.parseDouble(mat.group(1));
					count[current.length-1]=Integer.parseInt(mat.group(2));
					current[current.length-1]=Double.parseDouble(mat.group(5));
					int ipeek=(current.length)/2;
					if (findpeek(current,minimum_f)){
						System.out.println(count[ipeek]+"\t"+new DecimalFormat("0.000E00").format(time[ipeek])+"\t"
								+new DecimalFormat("0.000E00").format(current[ipeek]));
					}
				}
			}
			in.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
		
	}
	boolean findpeek(double[] current,boolean minimum_f){
	boolean stat=true;
	int ipeek=current.length/2;
	for (int i=0; i<current.length; i++){
		if (minimum_f){
			if (current[i]<current[ipeek]) {
				stat=false;
				break;
			}			
		}
		else {
			if (current[i]>current[ipeek]) {
				stat=false;
				break;
			}			
		}
	}
	return stat;
	}
}
