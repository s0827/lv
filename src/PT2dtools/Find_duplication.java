package PT2dtools;

import java.io.*;
import java.util.*;
import java.util.regex.*;

public class Find_duplication {							//	cluser.txtファイルによるセルの重複生成のチェック
	String dir="/Users/sasaki/exec/PT2dSF6/130522/";
	String did="0";
	String num="[0-9]+\\.[0-9]+";
	public static void main(String args[]){
		Find_duplication fd=new Find_duplication();
	}
	Find_duplication(){
		try {
			BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
			System.out.print("Enter data id ");
			String fnum=br.readLine();
			String id="PT-"+did+"-"+fnum;			
			String filename=dir+id+"/"+id+"-cluster.txt";
			analyze(filename);
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	void analyze(String filename){
		Pattern spat=Pattern.compile("step ([0-9]+) numcell [0-9]+");
		try {
			BufferedReader in=new BufferedReader(new FileReader(filename));
			while(true){
				String line=in.readLine();
				if (line==null) break;
				Matcher smat=spat.matcher(line);
				if (smat.find()){
					TreeMap<String, FDcount> map=analyze_clusters(in);
					int i=Integer.parseInt(smat.group(1));
					output(i,map);
				}
			}
			in.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	TreeMap<String, FDcount> analyze_clusters(BufferedReader in){
		TreeMap<String, FDcount> map=new TreeMap<String, FDcount>();
		String rnum="[0-9]+\\.[0-9]+";
		Pattern epat=Pattern.compile("numclusters [0-9]+ numcell [0-9]+ maxsize [0-9]+ avrsize "+rnum);
		Pattern cpat=Pattern.compile("[0-9]{6}");
		try {
			while(true){
				String line=in.readLine();
				Matcher emat=epat.matcher(line);
				if (emat.find()) break;
				Matcher cmat=cpat.matcher(line);
				while(cmat.find()){
					String key=cmat.group(0);
					FDcount C=map.get(key);
					if (C==null){
						map.put(key,new FDcount());
					}
					else {
						C.incr();
					}
				}
			}			
		}
		catch(Exception e){
			System.err.println(e);
		}
		return map;
	}
	void output(int i, TreeMap<String, FDcount> map){
		Iterator<String> it=map.keySet().iterator();
		System.out.print(i+" ("+map.size()+")");
		int icc=0;
		while(it.hasNext()){
			String key=it.next();
			int count=map.get(key).getCount();
			if (count>1){
				System.out.print(" "+key+"("+count+")");
				icc+=count;
			}
			else {
				icc++;
			}
		}
		System.out.println(" total="+icc);
	}
}

