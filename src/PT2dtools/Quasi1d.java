package PT2dtools;

import java.io.*;
import java.util.*;
import java.util.regex.*;
import java.text.*;

public class Quasi1d {
	String path="/Users/sasaki/data/originals/";	//	テスト結果の解析のため、id等の情報はハードコードする。
	int numfiles=1000;
	int numcell=10;
	String id="PT-0-";
	double[][] data;
	double[] time;
	public static void main(String args[]){
		Quasi1d q1d=new Quasi1d();
	}
	Quasi1d(){
		id+=get_id();
		data=new double[numfiles][numcell];
		time=new double[numfiles];
		for (int n=0; n<numfiles; n++){
			read_file(n);
		}
		try {
			PrintWriter out=new PrintWriter(new BufferedWriter(new FileWriter("/Users/sasaki/exec/PT2d/Quasi1d.txt")));
			out.print("time[s]");
//			for (int i=0; i<numcell; i++) out.print("\t"+i);
			for (int i=0; i<numcell; i++) out.print("\t");
			out.println();
			for (int n=0; n<numfiles; n++){
				out.print(new DecimalFormat("0.0000E00").format(time[n]));
				for (int i=0; i<numcell; i++){
					out.print("\t"+new DecimalFormat("0.0000E00").format(data[n][i]));
				}
				out.println();
			}
			out.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	String get_id(){
		String sid="";
		try {
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			System.out.print("Enter id : "+id);
			sid=in.readLine();
		}
		catch(Exception e){
			System.err.println(e);
		}
		return sid;
	}
	void read_file(int n){
		String inum="[0-9]+";
		String rnum="[\\-]{0,1}[0-9]\\.[0-9]{4}E[\\-]{0,1}[0-9]{2}";
		Pattern dpat=Pattern.compile(inum+" 0 ("+inum+") 0 ("+rnum+") "+rnum+" "+rnum+" "+inum);
//		Pattern dpat=Pattern.compile(inum+" 0 ("+inum+") 0 "+rnum+" "+rnum+" ("+rnum+") "+inum);
		Pattern tpat=Pattern.compile("time\\[s\\]= ([\\-\\.E0-9]+)");
		String filename=path+id+"/"+id+"-c-"+new DecimalFormat("000").format(n)+".txt";
		try {
			BufferedReader in=new BufferedReader(new FileReader(filename));
			in.readLine();
			String line=in.readLine();
			Matcher tmat=tpat.matcher(line);
			if (tmat.find()) time[n]=Double.parseDouble(tmat.group(1));
			while(true){
				line=in.readLine();
				if (line==null) break;
				Matcher dmat=dpat.matcher(line);
				if (dmat.find()){
					int iy=Integer.parseInt(dmat.group(1))-1;
					data[n][iy]=Double.parseDouble(dmat.group(2));
				}
			}
			in.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
}
