package PT2d;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.DecimalFormat;

import javax.imageio.ImageIO;

public class PT2dpj extends PT2dframe{						// main program
	PT2dparms parms;
	PT2dgenerator generator;
	PT2dmonitor monitor;
	PT2drenderer renderer;
	String path;
	
	public static void main(String args[]){
		PT2dpj PT2d=new PT2dpj();
	}
	PT2dpj(){
		String xmlfile="input_PT2d.xml";
		
		parms=new PT2dparms(xmlfile);						// read conditions
		PT2dDU du=new PT2dDU("PJ");
		path=du.create_directories();
		path+="/"+du.getDir();		
		du.copy_file(xmlfile);
		
		generator=null;
		generator=new GenSF6();								// program is only for SF6...
		generator.init(parms,path);
		
		PT2dpresetdata celldata=new PT2dpresetdata();
		if (parms.getInitial_condition_f()){
			String initial_file=parms.getInitial_file();
			celldata.read(initial_file);
			generator.setInitlist(celldata.getInitlist());
		}
		int Nx=parms.getNx();
		int Ny=parms.getNy();
		PT2dcell[][] cells=generator.initialize();
		Cluster cluster=new Cluster(Nx,Ny-2,0);		

		monitor=new PT2dmonitor(parms);
		monitor.help();
		
		renderer=setup_renderer(parms,cells);
		Thread t=new Thread(monitor);
		t.start();

		calculate(cells);
	}
	void calculate(PT2dcell[][] cells){
//		PT2dcalc solver=new PT2dcalc(parms);		
		PT2dc2 solver=new PT2dc2(parms);		
		solver.initialize();
		solver.set_r(cells);

		PT2dsummary summary=new PT2dsummary(parms,path,solver);
		double time=0.0;
		int i=0;

		double dt=parms.getTimestep();
		double dtw=parms.getMaxtime()/(double)parms.getOut_file();
		double wtime=dtw;
		int iwr=0;
		
		enable_paint_panel();
		while(true){
			generator.create_cell(dt,i);		//	セルの生成、消滅処理
			
			solver.shift();
			solver.set_r(cells);
			solver.calculate(dt);
			solver.calc_field(cells);			//	電界、電流、エネルギーの計算
			time+=dt;
			i++;
			
			if (i%parms.getOut_step()==0) {
				summary.output(i,time,dt,generator.get_numcell());
				display();
			}
			if (time>=wtime){
				output(cells,iwr,time,dt);
				dump(solver,iwr,time,dt);
				output_jpeg_file(iwr,path);
				wtime+=dtw;
				iwr++;
			}
			monitor.monitor();
			if (monitor.isStop_f()) break;
			if (time>parms.getMaxtime()) break;
		}
		summary.close();
		generator.getCluster().close();
	}

	void output(PT2dcell[][] cells,int iwr,double time,double dt){
		String cpath=path+"-c-"+new DecimalFormat("000").format(iwr)+".txt";
		try {
			PrintWriter out=new PrintWriter(new BufferedWriter(new FileWriter(cpath)));
			String d_=parms.get_delimiter();
			out.println("Statistics of e-field and current");
			out.println("time[s]= "+new DecimalFormat("0.00000E00").format(time));
			out.println("i"+d_+"x"+d_+"iy"+d_+"iz"+d_+"Current[A]"+d_+"E-field[V/m]"+d_+"V[V]"+d_+"cell");
			int Nx=parms.getNx();
			int Ny=parms.getNy();
			int i=0;
			int iz=0;
			for (int ix=0; ix<Nx; ix++){
				for (int iy=1; iy<Ny; iy++){
					int icc=0;
					PT2dcell cell=cells[ix][jm1(iy)];
					if (cell.is_conductor()) icc=1;
					out.println(i+d_+ix+d_+iy+d_+iz+d_+new DecimalFormat("0.0000E00").format(cell.getI())+d_
							+new DecimalFormat("0.0000E00").format(cell.getE())+d_
							+new DecimalFormat("0.0000E00").format(cell.getV())+d_+icc);
					i++;
				}			
			}
			out.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	int jm1(int j){ return j-1; }			//	配列a,bに値をpack,unpackする時に使う
	
	void dump(PT2dc2 solver, int iwr, double time, double dt){
		String dpath=path+"-d-"+new DecimalFormat("000").format(iwr)+".txt";
		try {
			PrintWriter out=new PrintWriter(new BufferedWriter(new FileWriter(dpath)));
			out.println("time[s]= "+new DecimalFormat("0.00000E00").format(time));
			int Nx=parms.getNx();
			int Ny=parms.getNy();
			double[][] V=solver.getV();
			double[][] Ix=solver.getIx();
			double[][] Qx=solver.getQx();
			double[][] Fx=solver.getFx();
			double[][] Iy=solver.getIy();
			double[][] Qy=solver.getQy();
			double[][] Fy=solver.getFy();
			
			int i=0;
			int iz=0;
			out.println("i ix iy iz V");
			for (int ix=0; ix<Nx; ix++){
				for (int iy=1; iy<Ny; iy++){
					out.println(i+" "+ix+" "+iy+" "+iz+" "+new DecimalFormat("0.0000E00").format(V[ix][jm1(iy)]));
					i++;
				}			
			}
			out.println();
			i=0;
			out.println("i ix iy iz Ix Qx Fx");
			for (int ix=0; ix<Nx-1; ix++){
				for (int iy=1; iy<Ny; iy++){
					out.println(i+" "+ix+" "+iy+" "+iz+" "+new DecimalFormat("0.0000E00").format(Ix[ix][jm1(iy)])+
							" "+new DecimalFormat("0.0000E00").format(Qx[ix][jm1(iy)])+
							" "+new DecimalFormat("0.0000E00").format(Fx[ix][jm1(iy)]));
					i++;					
				}
			}
			out.println();
			i=0;
			out.println("i ix iy iz Iy Qy Fy");
			for (int ix=0; ix<Nx; ix++){
				for (int iy=0; iy<Ny; iy++){
					out.println(i+" "+ix+" "+iy+" "+iz+" "+new DecimalFormat("0.0000000E00").format(Iy[ix][iy])+
							" "+new DecimalFormat("0.0000E00").format(Qy[ix][iy])+
							" "+new DecimalFormat("0.0000E00").format(Fy[ix][iy]));
					i++;					
				}
			}
			out.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	void output_jpeg_file(int i, String path){
		try {
			BufferedImage im=new BufferedImage(PT2dparms.framewidth,PT2dparms.frameheight,BufferedImage.TYPE_3BYTE_BGR);
			Graphics2D g2=(Graphics2D)im.getGraphics();
			renderer.draw(g2);
	//		File dir=new File(path+"/jpeg");
	//		if (!dir.exists()) dir.mkdir();
			File file=new File(path+"-g-"+new DecimalFormat("000").format(i)+".jpg");
			ImageIO.write(im,"jpeg",file);
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
}
