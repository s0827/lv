package PT2d;

import java.io.*;
import java.text.DecimalFormat;

public class PT2dsummary {
	PrintWriter summary;
	int gostep;
	PT2dparms parms;
	PT2dabstractd solver;
	PT2dsummary(PT2dparms parms, String path, PT2dabstractd solver){
		this.gostep=gostep;
		this.solver=solver;
		this.parms=parms;
		String d_=parms.get_delimiter();
		try {
			summary=new PrintWriter(new BufferedWriter(new FileWriter(path+"-summary.txt")));
			String write_str="time"+d_+"#"+d_+"numcell"+d_+"dt"+d_+"bottom"+d_+"current"+d_+"top"+d_+"current"+d_+"C_engy"+d_+"R_engy";
			summary.println(write_str);
			System.out.println(write_str);
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	void output(int i, double time, double dt, int numcell){
		String d_=parms.get_delimiter();
		String write_str=d_+new DecimalFormat("0.000E00").format(time)
					+d_+i
					
					+d_+numcell
					+d_+new DecimalFormat("0.000E00").format(dt)
					+d_+new DecimalFormat("0.000E00").format(solver.getI0())
					+d_+new DecimalFormat("0.000E00").format(solver.getIp())
					+d_+new DecimalFormat("0.000E00").format(solver.getEc())
					+d_+new DecimalFormat("0.000E00").format(solver.getEr());
		System.out.println("time[s]="+write_str);
		summary.println(write_str);
	}
	public void close(){
		summary.close();
	}
}
