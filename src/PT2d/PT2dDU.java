package PT2d;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.*;
import java.nio.file.*;

public class PT2dDU {
	String code="DT";							//	プレフィクスの省略値
	String master_dir="./";						//	デフォルトの親ディレクトリ
	String d_dir,dir,path;
	public PT2dDU(){
	}
	public PT2dDU(String code){
		this.code=code;
		Pattern pat=Pattern.compile("^[0-9]+$");
		try {
			BufferedReader in=new BufferedReader(new FileReader(master_dir+"host_id.txt"));
			while(true){
				String line=in.readLine();
				if (line==null) break;
				Matcher mat=pat.matcher(line);
				if (mat.find()) {
					this.code=code+"-"+mat.group(0)+"-";
					break;
				}
			}
			in.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	public String create_directories(){
		return create_directories(true);
	}
	public String create_directories(boolean du_f){
		dir=find_directory();								//	dir is determined as data id	
		path=master_dir+dir;
		try {
			File file=new File(path);						//	create directory to store data
			file.mkdirs();
		}
		catch(Exception e) {
			System.err.println(e);
		}
		return path;
	}
	String find_directory(){
		File odir=new File(master_dir);
		File[] ddirs=odir.listFiles();
		Pattern pat=Pattern.compile(code+"([0-9]+)");
		int maxnum=0;
		if (ddirs!=null){
			for (int i=0; i<ddirs.length; i++){
				String dname=ddirs[i].getName();
				Matcher mat=pat.matcher(dname);
				if (mat.find()) {
					maxnum=Math.max(maxnum,Integer.parseInt(mat.group(1)));
				}
			}			
		}
		String dir=code+new DecimalFormat("0000").format(maxnum+1);
		return dir;
	}
	public void copy_file(String src){			// copy input files to designated directory
		String dst=path+"/"+dir+"-"+src;
		try{
			Path src_file=Paths.get(src);
			Path dst_file=Paths.get(dst);
			Files.copy(src_file, dst_file);
		}
		catch(Exception e){
			System.err.println(e+" error occured during file copy");
		}
	}
	public String getPath(){ return path; }
	public String getDir(){ return dir; }
	public String getD_dir(){ return d_dir; }
}