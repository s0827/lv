package PT2d;

import java.text.DecimalFormat;

import org.apache.commons.math.linear.*;

public class PT2dc2 extends PT2dcd2 {
	RealMatrix a;
	RealVector b;
	double L=0.0e-8;
	double LF;

	PT2dc2(PT2dparms parms){
		this.parms=parms;
		Nx=parms.getNx();
		Ny=parms.getNy();
		Vp=parms.getVp();
		R=parms.get_r();
		C=parms.get_c();
		initialize_arrays();
		
		int num=totalmesh();
		a=new Array2DRowRealMatrix(num,num);
		b=new ArrayRealVector(num);

	}

	void initialize(){						//	静電容量、初期電位を設定	
		double dV=Vp/(double)(Ny);
		for (int ix=0; ix<Nx; ix++){
			for (int iy=1; iy<Ny; iy++) V[ix][jm1(iy)]=dV*(double)(iy);
		}
		for (int ix=0; ix<Nx; ix++){
			for (int iy=0; iy<Ny; iy++) Qy[ix][iy]=C*dV;
		}
	}
	void set_r(PT2dcell[][] cells){
		set(Fx,1.0);
		set(Fy,1.0);
		
		for (int ix=0; ix<Nx-1; ix++){
			for (int iy=1; iy<Ny-1; iy++){
				if (cells[ix][jm1(iy)].is_conductor() && cells[ix+1][jm1(iy)].is_conductor()) Fx[ix][jm1(iy)]=0.0;
			}
		}
		for (int ix=0; ix<Nx; ix++){
			if (cells[ix][jm1(1)].is_conductor()) Fy[ix][0]=0.0;
			for (int iy=1; iy<Ny-1; iy++) {
				if (cells[ix][jm1(iy)].is_conductor() && cells[ix][jm1(iy+1)].is_conductor()) Fy[ix][iy]=0.0;
			}
			if (cells[ix][jm1(Ny-1)].is_conductor()) Fy[ix][Ny-1]=0.0;
		}
	}
	int calculate(double dt){
		this.dt=dt;
		
		clear();
//		System.out.println("totalmesh "+num);
		
		int ic=0;

		ic++;
		calc_coefficients();			//	係数行列に値を与える
//		dump(null);
	
		DecompositionSolver solver = new LUDecompositionImpl(a).getSolver();
		RealVector x=solver.solve(b);
//		dump(x);

		get_results(x);					//	結果を取得する
		return ic;
	}
	void calc_coefficients(){
		int ieq=0;
		for (int ix=0; ix<Nx; ix++){
			for (int iy=1; iy<Ny; iy++){
				double bi=0.0;
				if (ix>0) {
					a.setEntry(ieq,nQx(ix-1,iy),1.0);
					bi+=-Qx0[ix-1][jm1(iy)];
				}
				if (ix<Nx-1) {
					a.setEntry(ieq,nQx(ix,iy),-1.0);
					bi+=Qx0[ix][jm1(iy)];
				}
				a.setEntry(ieq,nQy(ix,iy-1),1.0);
				bi+=-Qy0[ix][iy-1];
				a.setEntry(ieq,nQy(ix,iy),-1.0);
				bi+=Qy0[ix][iy];
				b.setEntry(ieq,bi);
				ieq++;
			}
		}
		for (int ix=1; ix<Nx; ix++){
			for (int iy=1; iy<Ny; iy++){
				a.setEntry(ieq,nV(ix-1,iy),2.0);
				a.setEntry(ieq,nV(ix,iy),-2.0);
				a.setEntry(ieq,nQx(ix-1,iy),(2.0*R/dt+Fx[ix-1][jm1(iy)]/C));
				b.setEntry(ieq,calc_bx(ix,iy));
				ieq++;
			}
		}
		for (int ix=0; ix<Nx; ix++){
			for (int iy=1; iy<Ny+1; iy++){
				if (iy==1){
					a.setEntry(ieq,nV(ix,iy),-2.0);
					a.setEntry(ieq,nQy(ix,iy-1),calc_a(ix,iy));
					b.setEntry(ieq,(2.0*R/dt-Fy[ix][iy-1]/C)*Qy0[ix][iy-1]);
				}
				else if (iy==Ny) {
					a.setEntry(ieq,nV(ix,iy-1),2.0);
					a.setEntry(ieq,nQy(ix,iy-1),calc_a(ix,iy));
					b.setEntry(ieq,2.0*Vp+(2.0*R/dt-Fy[ix][iy-1]/C)*Qy0[ix][iy-1]);
				}
				else {
					a.setEntry(ieq,nV(ix,iy-1),2.0);
					a.setEntry(ieq,nV(ix,iy),-2.0);
					a.setEntry(ieq,nQy(ix,iy-1),calc_a(ix,iy));
					b.setEntry(ieq,calc_by(ix,iy));
				}
				ieq++;
			}
		}
	}
	void clear(){
		for (int i=0; i<totalmesh(); i++){
			for (int j=0; j<totalmesh(); j++){
				a.setEntry(i,j,0.0);
			}
			b.setEntry(i,0.0);
		}
	}
	double calc_bx(int ix, int iy){
		return ((2.0*R/dt-Fx[ix-1][jm1(iy)]/C)*Qx0[ix-1][jm1(iy)]);
	}
	double calc_by(int ix, int iy){
		return ((2.0*R/dt-Fy[ix][iy-1]/C)*Qy0[ix][iy-1]);
	}
	double calc_a(int ix, int iy){ return 2.0*R/dt+Fy[ix][iy-1]/C; }
	
	void get_results(RealVector x){
		for (int i=0; i<Nx; i++){
			for (int j=1; j<Ny; j++) V[i][jm1(j)]=x.getEntry(nV(i,j));
		}
		for (int i=0; i<Nx-1; i++){
			for (int j=1; j<Ny; j++) Qx[i][jm1(j)]=x.getEntry(nQx(i,j));
		}
		for (int i=0; i<Nx; i++){
			for (int j=0; j<Ny; j++) Qy[i][j]=x.getEntry(nQy(i,j));
		}
	}
	boolean converge(){
		double diff=0.0;
		double ddmax=0.0;
		double relax=0.5;
		boolean stat=false;
		double[][] d=new double[Nx][Ny-1];
		for (int ix=0; ix<Nx; ix++){
			for (int iy=1; iy<Ny; iy++){
				double dd=Math.abs(V[ix][jm1(iy)]-V0[ix][jm1(iy)]);
				ddmax=Math.max(ddmax,dd);
				diff=Math.max(diff,dd/(V[ix][jm1(iy)]+V0[ix][jm1(iy)]));
				d[ix][jm1(iy)]=dd;
			}
		}
		if (ddmax<Vp/1000.0) stat=true;
		if (diff<0.001) stat=true;
		for (int ix=0; ix<Nx; ix++){
			for (int iy=1; iy<Ny; iy++) V0[ix][jm1(iy)]=V0[ix][jm1(iy)]+d[ix][jm1(iy)]*relax;
		}
//		System.out.println(ddmax+"  "+diff+"  "+V0[1][2]);
		return stat;
	}

	int nV(int i, int j){ return (Ny-1)*i+jm1(j); }
	int nQx(int i, int j){ return mV()+(Ny-1)*i+jm1(j); }
	int nQy(int i, int j){ return mV()+mQx()+Ny*i+j; }
	int mV(){ return Nx*(Ny-1); }
	int mQx(){ return (Nx-1)*(Ny-1); }
	int totalmesh(){ return Nx*(Ny-1)+(Nx-1)*(Ny-1)+Nx*Ny; }
	
	void dump(RealVector x){
		int num=totalmesh();
		for (int i=0; i<num; i++){
			for (int j=0; j<num; j++){
				System.out.print(df(a.getEntry(i,j))+"\t");
			}
			System.out.print(df(b.getEntry(i))+"\t");
			if (x!=null) System.out.print(df(x.getEntry(i)));
			System.out.println("");
		}
		System.out.println();
	}
	String df(double x){
		String s=new String(".");
		if (x!=0.0) {
			s=new DecimalFormat("0.00000E00").format(x);
			if (x>=0) s=" "+s;			
		}
		return s;
	}
}
