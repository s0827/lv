package PT2d;

import java.util.*;
import java.text.*;
/**
 *	セルの生成、消滅処理を行う。２次元化、回路方程式の解法の変化に対応する。
 */
public abstract class PT2dgenerator {
	PT2dparms parms;
	PT2dcell[][] cells;		//	計算用のデータ構造
	Cluster cluster;		//	クラスタの統計情報処理用
	Random rnd;
	int Nx,Ny;
	LinkedList<PT2dpresetdataitem> initlist=null;		//	初期電離領域、電極の設定
	
	PT2dgenerator(){
	}
	void init(PT2dparms parms, String path){
		this.parms=parms;
		Integer SEED=parms.getSEED();
		rnd=new Random();
		if (SEED!=null) rnd=new Random(SEED.intValue());
		
		Nx=parms.getNx();
		Ny=parms.getNy();

		cluster=new Cluster(Nx,Ny-1,1);					//　サイズNy-1に変更
		cluster.init_output_cluster(path);
	}
	PT2dcell[][] initialize(){
		cells=new PT2dcell[Nx][Ny-1];					//　サイズNy-1に変更
		for (int ix=0; ix<cells.length; ix++){
			for (int iy=0; iy<cells[ix].length; iy++){
				cells[ix][iy]=new PT2dcell(ix,iy);
			}
		}
		//	バリアの設置。y=0またはNy-1のセルをvacanciesから除外して電離が起こらないようにする。
		if (parms.getBarrier().equals("top") || parms.getBarrier().equals("both")){
			for (int ix=0; ix<Nx; ix++) cells[ix][Ny-2].set_barrier();
		}
		if (parms.getBarrier().equals("bottom") || parms.getBarrier().equals("both")){
			for (int ix=0; ix<Nx; ix++) cells[ix][0].set_barrier();
		}
				
		//	初期電離領域の設定。領域内に突き出した電極の設定
		
		if (initlist!=null) {
			for (PT2dpresetdataitem cell: initlist){
				int ix=cell.getIx();
				int iy=cell.getIy();
				cells[ix][iy].set_electrode();
				Cell c=new Cell(ix,iy,0);
				cluster.add_cell(c);
			}
		}
		return cells;
	}
	abstract void create_cell(double dt,int i);
	abstract double create_tree(double prob, TreeMap<Double, PT2dcell> tree);
	abstract double remove_tree(double prob, TreeMap<Double, PT2dcell> tree);

	TreeMap<Double, PT2dcell> normalize(TreeMap<Double, PT2dcell> tree, double total){
		TreeMap<Double, PT2dcell> ntree=new TreeMap<Double, PT2dcell>();
		Iterator<Double> it=tree.keySet().iterator();
		while(it.hasNext()){
			Double Key=it.next();
			PT2dcell cell=tree.get(Key);
			Double NKey=new Double(Key.doubleValue()/total);
			ntree.put(NKey,cell);
		}
		return ntree;
	}
	PT2dcell get_cell(TreeMap<Double, PT2dcell> tree){
		SortedMap<Double, PT2dcell> tail=tree.tailMap(new Double(rnd.nextDouble()));
		PT2dcell cell=null;
		if (tail.size()>0) cell=tail.get(tail.firstKey());
		return cell;
	}
	LinkedList<PT2dcell> get_cells(TreeMap<Double, PT2dcell> tree, double prob){
		LinkedList<PT2dcell> cells=new LinkedList<PT2dcell>();
		LinkedHashSet<String> set=new LinkedHashSet<String>();
		int np=(int)prob+1;
		int i=0;
		while(i<np){
			SortedMap<Double, PT2dcell> tail=tree.tailMap(new Double(rnd.nextDouble())+(double)(i));			
			if (tail.size()>0) {
				PT2dcell cell=tail.get(tail.firstKey());
				int ix=cell.getX();
				int iy=cell.getY();
				String id=new DecimalFormat("00").format(ix)+new DecimalFormat("00").format(iy);
				if (!set.contains(id)){
					set.add(id);
					cells.add(cell);
					i++;
				}
			}
			else {
				i++;
			}
		}
		return cells;
	}
/*	LinkedList<PT2dcell> get_cells(TreeMap<Double, PT2dcell> tree, double prob){	//	一列同時に電離する。チェック用
		SortedMap<Double, PT2dcell> tail=tree.tailMap(new Double(rnd.nextDouble()));
		PT2dcell cell=null;
		if (tail.size()>0) cell=tail.get(tail.firstKey());
		LinkedList<PT2dcell> cells=new LinkedList<PT2dcell>();
		if (cell==null) return cells;
		int y=cell.getY();
		Iterator<Double> it=tree.keySet().iterator();
		while(it.hasNext()){
			PT2dcell c=tree.get(it.next());
			if (c.getY()==y) cells.add(c);
		}
		return cells;
	}*/

	int get_numcell(){
		return cluster.get_numcell();
	}
	void setInitlist(LinkedList<PT2dpresetdataitem> initlist){ this.initlist=initlist; }
	Cluster getCluster(){ return cluster; }
}
