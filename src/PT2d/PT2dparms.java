package PT2d;

import java.io.*;
import java.util.*;
import java.text.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class PT2dparms {

//	parameters for graphics 
/** max number of cells  */	public static int max_cells=100;
/** default frame width  */	public static int framewidth=520;
/**	default frame height */	public static int frameheight=570;
/**	default panel width  */	public static int panelwidth=500;
/**	default panel height */	public static int panelheight=500;
/**	default panel loc(x) */	public static int px=10;
/**	default panel loc(y) */	public static int py=40;

/**	vaccuum permittivity */	public double eps0=8.85418782e-12;
	
/** true if DirectroyUtil is used */	boolean dutil_f=false;
/** true if initial profile of ionized area */			boolean initial_condition_f=false;
/**	name of the file that stores initial condition */	String initial_file;

/** number of x meshes */	int nx=0;
/** number of y meshes */	int ny=0;
/** x calculation size */	double rx=0.0;
/** y calculation size */	double ry=0.0;
/**	x mesh size */			double dx=0.0;
/**	y mesh size */			double dy=0.0;
/** applied voltage */		double Vp=0.0;
/** conductivity [ohm m]*/	double r=0.0;		//	CR serial circuit model for SF6
/** on conductivity */		double ron=0.0;
/** off conductivity */		double roff=0.0;
/**	relative permittivity */double eps=0.0;
/**	initial timestep */		double timestep;
/**	max calculation time */	double maxtime;
/**	freq. of output summary */	int out_step=10;
/** number of output files */	int out_file=1000;
/** prefence of delimiter */	String delimiter="s";
/** seed of random number, null for default */	Integer SEED=null;

/**	true for calculation of SF6 */		boolean sf6_f=false;
/**	ionization time */					double ionization_time;
/**	true to include ionization  */		boolean ef_f=false;
/** standard e-field */					double e0=1.0e+6;
/**	standard rate */					double rate0=3.5e+8;
/** rate multiplier */					double mult=0.01;
/**	minimum e-field to consider its effect */		double efmin=0.01;
/**	multiplier for percolation rate by e-field */	double ef_factor=2.0;		// not used
/** true to include recombination */		boolean recomb_f=false;
/** true to include effect of current */	boolean cr_f=false;
/**	steady state population	*/				double steady_pop=0.2;		// not used
/**	ratio between ion and recomb rate */	double r_factor=2.0;		// not used
/** standard current */						double j0=1.0e+3;
/** multiplier of recombination rate */		double cr_factor=0.0;		// not used
/**	barrier discharge mode */				String barrier="";

/**	parameters for graphics	 */ 
/**	true to display cells */		boolean disp_cell_f=false;
/**	true to display current */		boolean disp_current_f=true;
/**	true to display e-field */		boolean disp_efield_f=false;
/**	true to display voltage */		boolean disp_volt_f=false;
/**	min and max of current */		PT2dminmax i_minmax;
/**	min and max of e-field */		PT2dminmax e_minmax;
/**	min and max of voltage */		PT2dminmax v_minmax;
/**	current time */					double time;

boolean nodisp_f=false;

public PT2dparms(String path, boolean nodisp_f){
	this.nodisp_f=nodisp_f;
	read(path);
}

public PT2dparms(String path){
	read(path);
}
void read(String path){
		String str="";
		if (!nodisp_f) System.out.print("reading input datafile...  ");
		try {
			InputStream is=new FileInputStream(path);
			DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
			DocumentBuilder builder=factory.newDocumentBuilder();
			Document doc=builder.parse(is);
			Element rootElement=doc.getDocumentElement();
			NodeList du_list=rootElement.getElementsByTagName("DirectoryUtil");
			if (du_list.getLength()>0) {
				NamedNodeMap du_attr=du_list.item(0).getAttributes();
				if (du_attr.getNamedItem("mode")!=null){
					if (du_attr.getNamedItem("mode").getNodeValue().equals("true")) dutil_f=true;
				}
			}
			NodeList x_list=rootElement.getElementsByTagName("x");									//	メッシュ情報を取得
			nx=Integer.parseInt(get_attr(x_list,"mesh"));
			rx=Double.parseDouble(get_attr(x_list,"size"));
			NodeList y_list=rootElement.getElementsByTagName("y");
			ny=Integer.parseInt(get_attr(y_list,"mesh"));
			ry=Double.parseDouble(get_attr(y_list,"size"));

			NodeList initial_condition_list=rootElement.getElementsByTagName("initial_condition");	 // 初期値設定ファイルの設定
			NamedNodeMap initial_condition_attr=initial_condition_list.item(0).getAttributes();
			if (initial_condition_attr.getNamedItem("file")!=null){
				initial_file=initial_condition_attr.getNamedItem("file").getNodeValue();
				if (initial_file.length()>0) initial_condition_f=true;
			}
			Vp=Double.parseDouble(get_attr(initial_condition_list,"voltage"));
		
			NodeList cell_list=rootElement.getElementsByTagName("cell");
			Node x_node=cell_list.item(0);
			NamedNodeMap attr=x_node.getAttributes();
			Node r_item=attr.getNamedItem("r");
			if (r_item!=null) {
				r=Double.parseDouble(r_item.getNodeValue());
				ron=r;
				roff=r;
			}
			else {
				Node ron_item=attr.getNamedItem("ron");
				if (ron_item!=null) ron=Double.parseDouble(ron_item.getNodeValue());
				Node roff_item=attr.getNamedItem("roff");
				if (roff_item!=null) roff=Double.parseDouble(roff_item.getNodeValue());
			}
			
			eps=Double.parseDouble(get_attr(cell_list,"eps"));
			
			NodeList calc_list=rootElement.getElementsByTagName("calculation");						// set timestep
			timestep=Double.parseDouble(get_attr(calc_list,"timestep"));
			maxtime=Double.parseDouble(get_attr(calc_list,"maxtime"));
			
			NodeList output_list=rootElement.getElementsByTagName("output");						// set output control
			out_step=Integer.parseInt(get_attr(output_list,"step"));
			out_file=Integer.parseInt(get_attr(output_list,"file"));
			delimiter=get_attr(output_list,"delimiter").substring(0,1).toUpperCase();
			
			NodeList Seed_list=rootElement.getElementsByTagName("random");							// initialize random number
			String Seed_str=get_attr(Seed_list,"seed");
			SEED=null;
			if (Seed_str!=null) SEED=new Integer(Seed_str);	

			NodeList perc_parm_list=rootElement.getElementsByTagName("percolation");				// condition of percolation
			Node perc_parm_node=perc_parm_list.item(0);
			NodeList pcalc_list=((Element)perc_parm_node).getElementsByTagName("calculation");
			Node pcalc_node=pcalc_list.item(0);			
			NamedNodeMap pcalc_attr=pcalc_node.getAttributes();
			Node pcalc_mode_item=pcalc_attr.getNamedItem("mode");
//			if (pcalc_mode_item.getNodeName().equals("SF6")) {
				sf6_f=true;
//			}
				
//			parameters will not be provided when calculation for SF6 is assumed
			Node pcalc_e0_item=pcalc_attr.getNamedItem("e0");
			if (pcalc_e0_item != null ) e0=Double.parseDouble(pcalc_e0_item.getNodeValue());
			Node pcalc_rate0_item=pcalc_attr.getNamedItem("rate0");
			if (pcalc_rate0_item != null ) rate0=Double.parseDouble(pcalc_rate0_item.getNodeValue());
			Node pcalc_mult_item=pcalc_attr.getNamedItem("mult");
			if (pcalc_mult_item != null ) mult=Double.parseDouble(pcalc_mult_item.getNodeValue());
			
			NodeList ionization_list=((Element)perc_parm_node).getElementsByTagName("ionization");
			if (ionization_list.getLength()>0 ) {
				str=get_attr(ionization_list,"mode");
				if (str.equals("efield")) ef_f=true;
				efmin=Double.parseDouble(get_attr(ionization_list,"efmin"));				
			}
			
			NodeList recombination_list=((Element)perc_parm_node).getElementsByTagName("recombination");
			if (recombination_list.getLength()>0 ) {
				str=get_attr(recombination_list,"mode");
				if (str.equals("const")){
					recomb_f=true;
				}
				else if (str.equals("current")){
					recomb_f=true;
					cr_f=true;
				}
				r_factor=Double.parseDouble(get_attr(recombination_list,"factor"));		// cannot omit even not used...
				j0=Double.parseDouble(get_attr(recombination_list,"j0"));				
			}
			
			NodeList barrier_list=((Element)perc_parm_node).getElementsByTagName("barrier_discharge");	//	setting barrier
			str=get_attr(barrier_list,"mode");
			if (str!=null) barrier=str;

			is.close();
			dx=rx/(double)nx;
//			dy=ry/(double)(ny-2);
			dy=ry/(double)(ny-1);		//	for 2nd version...
		
			if (dx!=dy){
				if (!nodisp_f) System.out.println("mesh size must be the same for x,y axis ("+new DecimalFormat("0.00000E00").format(dx)+
						","+new DecimalFormat("0.00000E00").format(dy)+")");
				System.exit(0);
			}
			if (!nodisp_f){
				System.out.println("done");
				System.out.println("resitance of each element (on/off)[ohm]= "+new DecimalFormat("0.000E00").format(get_ron())+
						"/"+new DecimalFormat("0.000E00").format(get_roff()));
				System.out.println("capacity of each element [F]= "+new DecimalFormat("0.000E00").format(get_c()));				
			}
			
			/* preset graphics */	
			i_minmax=new PT2dminmax(0.0,Vp/(get_ron()*(double)(ny-2))*2.0);
			e_minmax=new PT2dminmax(0.0,Vp/ry*2.0);
			v_minmax=new PT2dminmax(0.0,Vp);
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	public void display_cell(){
		disable_flags();
		disp_cell_f=true;
	}
	public void display_current(){
		disable_flags();
		disp_current_f=true;
	}
	public void display_efield(){
		disable_flags();
		disp_efield_f=true;
	}
	public void display_volt(){
		disable_flags();
		disp_volt_f=true;
	}
	void disable_flags(){
		disp_cell_f=false;
		disp_current_f=false;
		disp_efield_f=false;
		disp_volt_f=false;
	}
	String get_attr(NodeList nlist, String str){
		Node x_node=nlist.item(0);
		NamedNodeMap attr=x_node.getAttributes();
		Node item=attr.getNamedItem(str);
		if (item==null) return null;
		return item.getNodeValue();
	}
	public double get_c(){ return eps0*eps*dx; }		// square element is assumed...
	public double get_r(){ return r/dx; }
	public double get_ron(){ return ron/dx; }
	public double get_roff(){ return roff/dx; }
	public String get_delimiter() {
		String D=" ";
		if (delimiter.equals("C")) D=", ";
		else if (delimiter.equals("T")) D="\t";
		return D;
	}
	public boolean getDutil_f(){ return dutil_f; }
	public boolean getInitial_condition_f(){ return initial_condition_f; }
	public String getInitial_file(){ return initial_file; }
	public int getNx(){ return nx; }
	public int getNy(){ return ny; }
	public double getRx(){ return rx; }
	public double getRy(){ return ry; }
	public double getDx(){ return dx; }
	public double getDy(){ return dy; }
	public double getVp(){ return Vp; }
	public double getR(){ return r; }
	public double getEps(){ return eps; }
	public double getRon(){ return ron; }
	public double getRoff(){ return roff; }
	public double getTimestep(){ return timestep; }
	public double getMaxtime(){ return maxtime; }
	public int getOut_step(){ return out_step; }
	public int getOut_file(){ return out_file; }
	public String getDelimiter() { return delimiter; }
	public Integer getSEED(){ return SEED; }
	public boolean getSf6_f(){ return sf6_f; }
	public double getMult(){ return mult; }
	public double getIonization_time(){ return ionization_time; }
	public boolean getEf_f(){ return ef_f; }
	public double getE0(){ return e0; }
	public double getRate0(){ return rate0; }
	public double getEfmin(){ return efmin; }
	public double getEf_factor(){ return ef_factor; }
	public boolean getRecomb_f(){ return recomb_f; }
	public boolean getCr_f(){ return cr_f; }
	public double getSteady_pop(){ return steady_pop; }
	public double getJ0(){ return j0; }
	public double getR_factor(){ return r_factor; }
	public double getCr_factor(){ return cr_factor; }
	public String getBarrier(){ return barrier; }
	
	public boolean getDisp_cell_f(){ return disp_cell_f; }
	public boolean getDisp_current_f(){ return disp_current_f; }
	public boolean getDisp_efield_f(){ return disp_efield_f; }
	public boolean getDisp_volt_f(){ return disp_volt_f; }
	public PT2dminmax getI_minmax(){ return i_minmax; }
	public PT2dminmax getE_minmax(){ return e_minmax; }
	public PT2dminmax getV_minmax(){ return v_minmax; }
	public double getTime(){ return time; }
}
