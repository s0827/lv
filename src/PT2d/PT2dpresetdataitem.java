package PT2d;

import java.util.StringTokenizer;

public class PT2dpresetdataitem {
	/** セルのx座標 */ int ix;
	/** セルのy座標 */ int iy;
	double time;
	/**	対象セルを生成、削除処理するかの指定。真で生成 */ 
	boolean cr_flag=false;					//	真で要素生成、偽で消滅
	/** 入力ファイルの1レコードから内容を取り出す。
	 * @param com 生成、削除処理のいずれかの指定
	 * @param tokens 入力ファイルの1レコード
	 */
	public PT2dpresetdataitem(String com, StringTokenizer tokens){
		if (com.equals("C")) cr_flag=true;
		ix=Integer.parseInt(tokens.nextToken());
		iy=Integer.parseInt(tokens.nextToken());
		tokens.nextToken();								// iz読飛ばす
		time=Double.parseDouble(tokens.nextToken());
	}
	public boolean getCr_flag(){ return cr_flag; }
	public int getIx(){ return ix; }
	public int getIy(){ return iy; }
	public double getTime(){ return time; }

}
