package PT2d;

public abstract class PT2dabstractd {
	PT2dabstractd(){
	}
	void clear(double[][] X){
		for (int ix=0; ix<X.length; ix++){
			for (int iy=0; iy<X[ix].length; iy++) X[ix][iy]=0.0;
		}
	}
	public void set(double[][] X, double D){
		for (int ix=0; ix<X.length; ix++){
			for (int iy=0; iy<X[ix].length; iy++) X[ix][iy]=D;
		}
	}
	void shift(double[][] X, double[][] X0){
		for (int ix=0; ix<X.length; ix++){
			for (int iy=0; iy<X[ix].length; iy++) X0[ix][iy]=X[ix][iy];
		}		
	}
	int jm1(int j){ return j-1; }			//	配列a,bに値をpack,unpackする時に使う
	abstract double getI0() ;
	abstract double getIp() ;
	abstract double getEc() ;
	abstract double getEr() ;
}
