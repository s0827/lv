package PT2d;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.LinkedList;

public class Cluster {
	int nx,ny,nz;
	LinkedList<Cell> cell_list;
	Cell[][][] cells;
	PrintWriter out;
	Cluster(int nx, int ny, int nz){
		this.nx=nx;
		this.ny=ny;
		this.nz=nz;
		cells=new Cell[nx][ny][nz];
		for (int ix=0; ix<nx; ix++){
			for (int iy=0; iy<ny; iy++){
				for (int iz=0; iz<nz; iz++) cells[ix][iy][iz]=null;
			}
		}
		cell_list=new LinkedList<Cell>();
	}
	void add_cell(Cell cell){
		out.print(" "+new DecimalFormat("000000").format(cell.getId()));
		new_cell(cell);										//	接続状態を更新
		cell_list.add(cell);								//	リストに追加する
	}

	/** 一個のプラズマ化したセルを追加し、隣接するセルを含めて接続状態を更新する。
 * @param cell 新しく追加するセル
 * @param neighbors 新しく追加するセルに隣接するセルのリスト
 */
	void new_cell(Cell cell){
		int ix=cell.getIx();
		int iy=cell.getIy();
		int iz=cell.getIz();
		cells[ix][iy][iz]=cell;
		LinkedList<Cell> neighbors=get_neighbors(ix,iy,iz);
		if (neighbors.size()!=0){
			Iterator<Cell> it=neighbors.iterator();						//	最初に見つかった要素の下位につける
			Cell neighbor=it.next();
			cell.setParent(neighbor);
			neighbor.add_child(cell);
			Cell head=neighbor.get_head();
			while(it.hasNext()){										//	さらに接続している要素がある時は、クラスタを連結する処理をする
				Cell next=it.next();
				Cell next_head=next.get_head();
				if (head!=next_head){
//					if (head.getId()!=next_head.getId()){
//						System.out.println(head.getId()+" "+next_head.getId()+" ?");
					cell.add_child(next);
					next.connect(cell);									//	見つかったクラスタを、cell以下に接続する							
				}
			}
		}
	}
	/** 座標で指定されたセルに隣接するセルを探し、リストを作成する。
	 * @param ix セルのx座標
	 * @param iy セルのy座標
	 * @param iz セルのz座標
	 * @return 入力セルに隣接するCellのリスト
	 */
	LinkedList<Cell> get_neighbors(int ix, int iy, int iz){				//	周辺のセルに要素が存在するか調べる
		LinkedList<Cell> neighbors=new LinkedList<Cell>();
		if (ix<nx-1) if (cells[ix+1][iy][iz]!=null) neighbors.add(cells[ix+1][iy][iz]);
		if (ix>0)    if (cells[ix-1][iy][iz]!=null) neighbors.add(cells[ix-1][iy][iz]);
		if (iy<ny-1) if (cells[ix][iy+1][iz]!=null) neighbors.add(cells[ix][iy+1][iz]);
		if (iy>0)    if (cells[ix][iy-1][iz]!=null) neighbors.add(cells[ix][iy-1][iz]);
		if (iz<nz-1) if (cells[ix][iy][iz+1]!=null) neighbors.add(cells[ix][iy][iz+1]);
		if (iz>0)    if (cells[ix][iy][iz-1]!=null) neighbors.add(cells[ix][iy][iz-1]);
		return neighbors;
	}
	void remove_cell(Cell cell){
//		if (cell!=null){
			out.print(" "+new DecimalFormat("000000").format(cell.getId()));
			remove(cell);
			cell_list.remove(cell);
//		}
	}
	Cell remove(Cell cell){
		int ix=cell.getIx();
		int iy=cell.getIy();
		int iz=cell.getIz();
		Cell parent=cell.getParent();
		if (parent!=null) parent.remove_child(cell);			//	親要素から子を除去する
		LinkedList<Cell> childs=cell.getChilds();
		for (Cell child: childs) child.remove_parent();			//	子要素から親を除去する。先頭セルとなる。
		cells[ix][iy][iz]=null;									//	消去すべきセルから親子の情報を取得し、処理を行なったあとで、配列およびリストから要素を消去
		cell_list.remove(cell);
		for (Cell child: childs){								//	新しい先頭セル以下の要素が、元の親や元の子に接続していないかを調べる。
			if (child.is_head()) {								//	接していて接続されていないものは、元の親か元の子なので調べる必要はない。
				child.find_reconnection(cells,child,nx,ny,nz);	//	自分自身と接続してはいけない
			}
		}
		return cell;
	}
/** 現在のクラスタの接続状態をコンソールに出力する。
 * @param i 現在の計算ステップ
 */
	void init_output_cluster(String path){
		out=null;
		try {
			out=new PrintWriter(new BufferedWriter(new FileWriter(path+"-cluster.txt")));
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	void output_cluster(int i){									//	クラスターの情報を出力
		out.println("step "+i+" numcell "+cell_list.size());
		int ic=0;
		int sum=0;
		int maxcluster=0;
		for (Cell cell: cell_list){
			if (cell.is_head()){
				out.print(ic+"> ");
				int numcell=cell.output_cluster(out,true);		//	再帰的にセルのIDを出力し、トータルのセル数を返す
				out.println(" ("+numcell+")");
				if (maxcluster<numcell) maxcluster=numcell;
				sum+=numcell;
				ic++;
			}
		}
		double rsum=sum;
		double ric=ic;
		double size=rsum/ric;
		out.println("numclusters "+ic+" numcell "+sum+" maxsize "+maxcluster+" avrsize "+new DecimalFormat("0.0000").format(size));
		out.println();
	}
	void out(String s){ out.println(s); }
	void outnocr(String s){ out.print(s); }
	void close(){
		out.close();
	}
	Cell get_cell(int ix, int iy, int iz){ return cells[ix][iy][iz]; }
	int get_numcell(){ return cell_list.size(); }
}
