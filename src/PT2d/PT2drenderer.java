package PT2d;

import java.awt.Color;
import java.awt.Graphics2D;

public class PT2drenderer {
	PT2dparms parms;
	PT2dcell[][] cells;
	PT2drenderer(PT2dparms parms, PT2dcell[][] cells) {
		this.parms=parms;
		this.cells=cells;
	}
	void draw(Graphics2D g2){
		PT2dminmax mx=parms.getI_minmax();
		if (parms.getDisp_current_f()) mx=parms.getI_minmax();
		else if (parms.getDisp_efield_f()) mx=parms.getE_minmax();
		else if (parms.getDisp_volt_f()) mx=parms.getV_minmax();
		
		int Nx=parms.getNx();
		int Ny=parms.getNy();
		double dx=(double)PT2dparms.panelwidth/(double)Nx;
		double dy=(double)PT2dparms.panelheight/(double)(Ny-1);
		int w=(int)dx;
		int h=(int)dy;
		if (!parms.getDisp_cell_f()){

			g2.setColor(Color.black);			
			g2.fillRect(0,0,PT2dparms.panelwidth,PT2dparms.panelheight);

			double dmin=1.0;
			double dmax=0.0;
			for (int ix=0; ix<Nx; ix++){
				for (int iy=1; iy<Ny; iy++){
					int px=(int)(dx*ix);
					int py=PT2dparms.panelheight-(int)(dy*(iy));
					double val=0.0;
					if (parms.getDisp_current_f()) val=cells[ix][jm1(iy)].getI();
					else if (parms.getDisp_efield_f()) val=cells[ix][jm1(iy)].getE();
					else if (parms.getDisp_volt_f()) val=cells[ix][jm1(iy)].getV();
					double d=mx.get(val);
					if (dmin>d) dmin=d;
					if (dmax<d) dmax=d;
//					System.out.print(new DecimalFormat("0.0E00").format(equations[ix][iy][iz].getCurrent())+" ");
					int r=0;
					int g=0;
					int b=0;
					if (d>0.0 && d<0.25){
						g=(int)(d/0.25*255.0);
						b=(int)(d/0.25*255.0);
					}
					else if (d>=0.25 && d<0.5){
						r=(int)((d-0.25)/0.25*255.0);
						b=(int)((0.5-d)/0.25*255.0);
						g=255;
					}
					else if (d>=0.5 && d<0.75){
						r=255;
						g=(int)((0.75-d)/0.25*255.0);
						b=(int)((d-0.5)/0.25*255.0);
					}
					else if (d>=0.75 && d<1.0){
						r=255;
						g=(int)((d-0.75)/0.25*255.0);
						b=255;
					}
					else if (d>=1.0){
						r=255;
						g=255;
						b=255;
					}
					Color c=new Color(r,g,b);
					g2.setColor(c);
					g2.fillRect(px,py,w,h);
				}
//				System.out.println();
			}			
		}
		for (int ix=0; ix<Nx; ix++){
			for (int iy=1; iy<Ny; iy++){
				if (cells[ix][jm1(iy)].is_plasma() || cells[ix][jm1(iy)].is_conductor()){
					int px=(int)(dx*ix);
					int py=PT2dparms.panelheight-(int)(dy*iy);
					if (parms.getDisp_cell_f()){
						g2.setColor(Color.gray);
						g2.fillRect(px,py,w,h);												
						g2.setColor(Color.white);
						g2.drawRect(px,py,w,h);												
					}
					else {
						g2.setColor(Color.white);
						g2.drawRect(px,py,w,h);						
					}
				}
			}
		}
	}
	int jm1(int j){ return j-1; }			//	配列a,bに値をpack,unpackする時に使う
}
