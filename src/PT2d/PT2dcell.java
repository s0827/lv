package PT2d;

public class PT2dcell {
	boolean plasma_f=false;			//	プラズマ化して、低抵抗となる時真、
	boolean electrode_f=false;		//	真の時電極として扱う。常に低抵抗で高抵抗にならない。
	boolean barrier_f=false;		//	真の時バリアとして扱う。常に高抵抗でプラズマ化しない。
	double V,Ix,Iy,Ex,Ey;			//	電位、電界、電流
	int x,y;
	PT2dcell(int x, int y){
		this.x=x;
		this.y=y;
	}
	void set_electrode(){
		electrode_f=true;
	}
	void set_barrier(){
		barrier_f=true;
		plasma_f=false;
	}
	void setV(double V){ this.V=V; }
	void setIx(double Ix){ this.Ix=Ix; }
	void setIy(double Iy){ this.Iy=Iy; }
	void setEx(double Ex){ this.Ex=Ex; }
	void setEy(double Ey){ this.Ey=Ey; }
	
	double getI(){ return Math.sqrt(Ix*Ix+Iy*Iy); }
	double getE(){ return Math.sqrt(Ex*Ex+Ey*Ey); }
	
	void ionize(){ 
		if (!barrier_f) plasma_f=true;
	}
	void recombine(){
		if (!electrode_f) plasma_f=false;
	}
	boolean is_neutral(){
		boolean stat=false;
		if (!barrier_f && !plasma_f && !electrode_f) stat=true;
		return stat;
	}
	boolean is_plasma(){
		boolean stat=false;
		if (!electrode_f && plasma_f) stat=true;
		return stat;
	}
	boolean is_conductor(){
		boolean stat=false;
		if (electrode_f || plasma_f) stat=true;
		return stat;
	}
	int getX(){ return x; }
	int getY(){ return y; }
	double getV(){ return V; }
	double getIx(){ return Ix; }
	double getIy(){ return Iy; }
	double getEx(){ return Ex; }
	double getEy(){ return Ey; }
}
