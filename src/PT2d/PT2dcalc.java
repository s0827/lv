package PT2d;

import org.apache.commons.math.linear.*;
import java.text.*;

public class PT2dcalc extends PT2dcalcdata {
	RealMatrix a;
	RealVector b;
	double Vp;
	double r=0.0;

	PT2dcalc(){}
	PT2dcalc(PT2dparms parms){				//	コードを小さくするため、配列を親クラスに置く。
		this.parms=parms;
		Vp=parms.getVp();
		r=-parms.get_ron();
		initialize_arrays();
	}
	
	void initialize(){						//	静電容量、初期電位を設定	
		for (int ix=0; ix<Nx-1; ix++){
			for (int iy=1; iy<Ny-1; iy++) Cx[ix][jm1(iy)]=parms.get_c();
		}
		for (int ix=0; ix<Nx; ix++){
			for (int iy=0; iy<Ny-1; iy++) Cy[ix][iy]=parms.get_c();
		}
		double dV=Vp/(double)(Ny-1);
		for (int ix=0; ix<Nx; ix++){
			for (int iy=1; iy<Ny-1; iy++) V[ix][jm1(iy)]=dV*(double)(iy);
		}
		for (int ix=0; ix<Nx; ix++){
			for (int iy=0; iy<Ny-1; iy++) Qy[ix][iy]=Cy[ix][iy]*dV;
		}
	}
	
	void set_r(PT2dcell[][] cells){
		double ron=parms.get_ron();
		double roff=parms.get_roff();
		set(Rx,roff);
		set(Ry,roff);
		for (int ix=0; ix<Nx-1; ix++){
			for (int iy=1; iy<Ny-1; iy++){
				if (cells[ix][jm1(iy)].is_plasma() && cells[ix+1][jm1(iy)].is_plasma()) Rx[ix][jm1(iy)]=ron;
			}
		}
		for (int ix=0; ix<Nx; ix++){
			if (cells[ix][jm1(1)].is_plasma()) Ry[ix][0]=ron;
			for (int iy=1; iy<Ny-2; iy++) {
				if (cells[ix][jm1(iy)].is_plasma() && cells[ix][jm1(iy+1)].is_plasma()) Ry[ix][iy]=ron;
			}
			if (cells[ix][jm1(Ny-2)].is_plasma()) Ry[ix][Ny-2]=ron;
		}
	}
	
	void calculate(double dt){
		this.dt=dt;
		
		int num=totalmesh();
//		System.out.println("totalmesh "+num);
		a=new Array2DRowRealMatrix(num,num);
		b=new ArrayRealVector(num);
		
		calc_coefficients();			//	係数行列に値を与える
//		dump(null);
	
		DecompositionSolver solver = new LUDecompositionImpl(a).getSolver();
		RealVector x=solver.solve(b);
//		dump(x);

		get_results(x);					//	結果を取得する
	}
	void calc_coefficients(){			//	項毎に境界条件を考慮
		int ieq=0;
		Double d;
		for (int ix=0; ix<Nx; ix++){
			for (int iy=1; iy<Ny-1; iy++){
				double bi=0.0;
				double aij=0.0;
				if (ix>0) {
					a.setEntry(ieq,nV(ix-1,iy),-0.5/Rx[ix-1][jm1(iy)]);
					aij+=0.5/Rx[ix-1][jm1(iy)];
					bi+=-calc_bx(ix,iy);
				}
				if (ix<Nx-1){
					a.setEntry(ieq,nV(ix+1,iy),-0.5/Rx[ix][jm1(iy)]);
					aij+=0.5/Rx[ix][jm1(iy)];
					bi+=calc_bx(ix+1,iy);
				}
				if (iy==1){
					aij+=0.5/Ry[ix][iy-1];
					bi+=-0.5*V0[ix][jm1(iy)]/Ry0[ix][iy-1];
				}
				else {
					a.setEntry(ieq,nV(ix,iy-1),-0.5/Ry[ix][iy-1]);
					aij+=0.5/Ry[ix][iy-1];
					bi+=-calc_by(ix,iy);
				}
				if (iy==Ny-2){
					aij+=0.5/Ry[ix][iy];
					bi+=0.5*(Vp/Ry[ix][iy]+(Vp-V0[ix][jm1(iy)])/Ry0[ix][iy]);
				}
				else {
					a.setEntry(ieq,nV(ix,iy+1),-0.5/Ry[ix][iy]);
					aij+=0.5/Ry[ix][iy];
					bi+=calc_by(ix,iy+1);
				}
				a.setEntry(ieq,nV(ix,iy),aij);
				
				if (ix>0){
					a.setEntry(ieq,nQx(ix-1,iy),1.0/dt);
					bi+=-Qx0[ix-1][jm1(iy)]/dt;
				}
				if (ix<Nx-1){
					a.setEntry(ieq,nQx(ix,iy),-1.0/dt);
					bi+=Qx0[ix][jm1(iy)]/dt;
				}
				
				a.setEntry(ieq,nQy(ix,iy-1),1.0/dt);
				bi+=-Qy0[ix][iy-1]/dt;
				a.setEntry(ieq,nQy(ix,iy),-1.0/dt);
				bi+=Qy0[ix][iy]/dt;
				b.setEntry(ieq,bi);
				ieq++;
			}
		}
//		System.out.println(ieq);
		for (int ix=0; ix<Nx-1; ix++){
			for (int iy=1; iy<Ny-1; iy++){
				a.setEntry(ieq,nV(ix+1,iy),1.0);
				a.setEntry(ieq,nV(ix,iy),-1.0);
				a.setEntry(ieq,nQx(ix,iy),(2.0*r/dt-1.0/Cx[ix][jm1(iy)]));
				b.setEntry(ieq,V0[ix][jm1(iy)]-V0[ix+1][jm1(iy)]+(2.0*r/dt+1.0/Cx[ix][jm1(iy)])*Qx0[ix][jm1(iy)]);
				ieq++;
			}
		}
		for (int ix=0; ix<Nx; ix++){
			for (int iy=0; iy<Ny-1; iy++){
				if (iy==0){
					a.setEntry(ieq,nV(ix,iy+1),1.0);
					a.setEntry(ieq,nQy(ix,iy),(2.0*r/dt-1.0/Cy[ix][iy]));
					b.setEntry(ieq,-V0[ix][jm1(iy+1)]+(2.0*r/dt+1.0/Cy[ix][iy])*Qy0[ix][iy]);
				} 
				else if (iy==Ny-2){
					a.setEntry(ieq,nV(ix,iy),-1.0);
					a.setEntry(ieq,nQy(ix,iy),(2.0*r/dt-1.0/Cy[ix][iy]));
					b.setEntry(ieq,-2.0*Vp+V0[ix][jm1(iy)]+(2.0*r/dt+1.0/Cy[ix][iy])*Qy0[ix][iy]);					
				}
				else {
					a.setEntry(ieq,nV(ix,iy+1),1.0);
					a.setEntry(ieq,nV(ix,iy),-1.0);
					a.setEntry(ieq,nQy(ix,iy),(2.0*r/dt-1.0/Cy[ix][iy]));
					b.setEntry(ieq,-V0[ix][jm1(iy+1)]+V0[ix][jm1(iy)]+(2.0*r/dt+1.0/Cy[ix][iy])*Qy0[ix][iy]);
				}
				ieq++;
			}
		}
		
/*		for (int ix=1; ix<Nx; ix++){
			for (int iy=1; iy<Ny-1; iy++){
				a.setEntry(ieq,nV(ix-1,iy),1.0);
				a.setEntry(ieq,nV(ix,iy),-1.0);
				a.setEntry(ieq,nQx(ix-1,iy),1.0/Cx[ix-1][jm1(iy)]);
				b.setEntry(ieq,-V0[ix-1][jm1(iy)]+V0[ix][jm1(iy)]-Qx0[ix-1][jm1(iy)]/Cx[ix-1][jm1(iy)]);
				ieq++;
			}
		}
//		System.out.println(ieq);
		for (int ix=0; ix<Nx; ix++){
			for (int iy=1; iy<Ny-1; iy++){
				if (iy==1){
					a.setEntry(ieq,nV(ix,iy),-1.0);
					a.setEntry(ieq,nQy(ix,iy-1),1.0/Cy[ix][iy-1]);
					b.setEntry(ieq,V0[ix][jm1(iy)]-Qy0[ix][iy-1]/Cy[ix][iy-1]);
					ieq++;
				}
				else {
					a.setEntry(ieq,nV(ix,iy-1),1.0);
					a.setEntry(ieq,nV(ix,iy),-1.0);
					a.setEntry(ieq,nQy(ix,iy-1),1.0/Cy[ix][iy-1]);
					b.setEntry(ieq,-V0[ix][jm1(iy-1)]+V0[ix][jm1(iy)]-Qy0[ix][iy-1]/Cy[ix][iy-1]);
					ieq++;
				}
				if (iy==Ny-2){
					a.setEntry(ieq,nV(ix,iy),1.0);
					a.setEntry(ieq,nQy(ix,iy),1.0/Cy[ix][iy]);
					b.setEntry(ieq,-V0[ix][jm1(iy)]+2.0*Vp-Qy0[ix][iy]/Cy[ix][iy]);
					ieq++;				
				}
			}
		}*/
//		System.out.println(ieq);
	}
	double calc_bx(int i, int j){ return 0.5*(V0[i][jm1(j)]-V0[i-1][jm1(j)])/Rx0[i-1][jm1(j)]; } // クラス変数を渡さない
	double calc_by(int i, int j){ return 0.5*(V0[i][jm1(j)]-V0[i][jm1(j-1)])/Ry0[i][j-1]; }
	
	void get_results(RealVector x){
		for (int i=0; i<Nx; i++){
			for (int j=1; j<Ny-1; j++) V[i][jm1(j)]=x.getEntry(nV(i,j));
		}
		for (int i=0; i<Nx-1; i++){
			for (int j=1; j<Ny-1; j++) Qx[i][jm1(j)]=x.getEntry(nQx(i,j));
		}
		for (int i=0; i<Nx; i++){
			for (int j=0; j<Ny-1; j++) Qy[i][j]=x.getEntry(nQy(i,j));
		}
	}
	
	int nV(int i, int j){ return (Ny-2)*i+jm1(j); }
	int nQx(int i, int j){ return mV()+(Ny-2)*i+jm1(j); }
	int nQy(int i, int j){ return mV()+mQx()+(Ny-1)*i+j; }
	int mV(){ return Nx*(Ny-2); }
	int mQx(){ return (Nx-1)*(Ny-2); }
	int totalmesh(){ return Nx*(Ny-2)+(Nx-1)*(Ny-2)+Nx*(Ny-1); }
	
	void dump(RealVector x){
		int num=totalmesh();
		for (int i=0; i<num; i++){
			for (int j=0; j<num; j++){
				System.out.print(df(a.getEntry(i,j))+"\t");
			}
			System.out.print(df(b.getEntry(i))+"\t");
			if (x!=null) System.out.print(df(x.getEntry(i)));
			System.out.println("");
		}
	}
	String df(double x){
		String s=new String(".");
		if (x!=0.0) {
			s=new DecimalFormat("0.000000E00").format(x);
			if (x>=0) s=" "+s;			
		}
		return s;
	}
	
	void testnum(){
		Nx=3;
		Ny=5;
		int ii=0;
		System.out.println("ii\t\tix\tiy\tidx");
		for (int i=0; i<Nx; i++){
			for (int j=1; j<Ny-1; j++){
				System.out.println(ii+"\tV\t"+i+"\t"+j+"\t"+nV(i,j));
				ii++;
			}
		}
		for (int i=0; i<Nx-1; i++){
			for (int j=1; j<Ny-1; j++){
				System.out.println(ii+"\tQx\t"+i+"\t"+j+"\t"+nQx(i,j));
				ii++;
			}
		}
		for (int i=0; i<Nx; i++){
			for (int j=0; j<Ny-1; j++){
				System.out.println(ii+"\tQy\t"+i+"\t"+j+"\t"+nQy(i,j));
				ii++;
			}
		}
	}
	public static void main(String args[]){
		PT2dcalc pc=new PT2dcalc();
		pc.testnum();
	}
}
