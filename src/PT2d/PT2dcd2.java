package PT2d;

public class PT2dcd2 extends PT2dabstractd {
	PT2dparms parms;
	int Nx,Ny;
	double[][] V,Ix,Iy;			//	計算する回路のパラメータ
	double[][] V0,Ix0,Iy0;		//	前ステップの値
	double[][] Qx,Qy,Qx0,Qy0;	//	ノード電荷（電流の積分値）
	double[][] Fx,Fy;			//	接続状況制御のフラグ
	double R,C;					//	コンデンサの蓄積エネルギー、抵抗の消費エネルギー
	double I0,Ip,Ec,Er;			//	極板の電流。エネルギー
	double dt;
	double Vp;
	PT2dcd2(){}
	void initialize_arrays(){
		
		V=new double[Nx][Ny-1];
		Ix=new double[Nx-1][Ny-1];
		Iy=new double[Nx][Ny];
		
		V0=new double[Nx][Ny-1];
		Ix0=new double[Nx-1][Ny-1];
		Iy0=new double[Nx][Ny];

		Qx=new double[Nx-1][Ny-1];
		Qy=new double[Nx][Ny];
		Qx0=new double[Nx-1][Ny-1];
		Qy0=new double[Nx][Ny];

		Fx=new double[Nx-1][Ny-1];
		Fy=new double[Nx][Ny];
		
		clear(V);
		clear(Ix);
		clear(Iy);
		clear(V0);
		clear(Ix0);
		clear(Iy0);
		
		clear(Qx);
		clear(Qy);
		clear(Qx0);
		clear(Qy0);

		clear(Fx);
		clear(Fy);
	}
	
	void shift(){
		shift(V,V0);
		shift(Ix,Ix0);
		shift(Iy,Iy0);
		shift(Qx,Qx0);
		shift(Qy,Qy0);
	}
	void copy(double[][] X, double[][] Y){
		shift(X,Y);
	}
	void calc_field(PT2dcell[][] cells){
		double dx=parms.getDx();
		double dy=parms.getDy();
		for (int ix=0; ix<Nx; ix++){
			for (int iy=1; iy<Ny; iy++) cells[ix][jm1(iy)].setV(V[ix][jm1(iy)]);
		}
		double[][] Ex=calc_Ex();
		double[][] Ey=calc_Ey();
		
		calc_current();
		
		set_EI(cells,Ex,Ey,Ix,Iy);
		
		I0=0.0;
		Ip=0.0;
		for (int ix=0; ix<Nx; ix++) {
			I0+=Iy[ix][0];
			Ip+=Iy[ix][Ny-1];
		}
		calc_energy();
	}
	void calc_current(){
		for (int ix=0; ix<Nx-1; ix++){
			for (int iy=1; iy<Ny; iy++) Ix[ix][jm1(iy)]=(Qx[ix][jm1(iy)]-Qx0[ix][jm1(iy)])/dt;
		}
		for (int ix=0; ix<Nx; ix++){
			for (int iy=0; iy<Ny; iy++) Iy[ix][iy]=(Qy[ix][iy]-Qy0[ix][iy])/dt;
		}
	}
	double[][] calc_Ex(){
		double[][] Ex=new double[Nx-1][Ny-1];
		double dx=parms.getDx();
		for (int ix=0; ix<Nx-1; ix++){
			for (int iy=1; iy<Ny; iy++){
				Ex[ix][jm1(iy)]=(V[ix+1][jm1(iy)]-V[ix][jm1(iy)])/dx;
			}
		}
		return Ex;
	}
	double[][] calc_Ey(){
		double [][] Ey=new double[Nx][Ny];
		double dy=parms.getDy();
		for (int ix=0; ix<Nx; ix++){
			Ey[ix][0]=V[ix][jm1(1)]/dy;
			Ey[ix][Ny-1]=(Vp-V[ix][jm1(Ny-1)])/dy;
			for (int iy=1; iy<Ny-1; iy++){
				Ey[ix][iy]=(V[ix][jm1(iy+1)]-V[ix][jm1(iy)])/dy;
			}
		}
		return Ey;
	}	

	void set_EI(PT2dcell[][] cells, double[][] Ex, double[][] Ey, double[][] Ix, double[][] Iy){
		for (int iy=1; iy<Ny; iy++){
			cells[0][jm1(iy)].setEx(Ex[0][jm1(iy)]);
			cells[0][jm1(iy)].setIx(Ix[0][jm1(iy)]);
			for (int ix=1; ix<Nx-1; ix++){
				cells[ix][jm1(iy)].setEx((Ex[ix-1][jm1(iy)]+Ex[ix][jm1(iy)])/2.0);
				cells[ix][jm1(iy)].setIx((Ix[ix-1][jm1(iy)]+Ix[ix][jm1(iy)])/2.0);
			}
		}
		for (int ix=0; ix<Nx; ix++){
			for (int iy=1; iy<Ny; iy++){
				cells[ix][jm1(iy)].setEy((Ey[ix][iy-1]+Ey[ix][iy])/2.0);
				cells[ix][jm1(iy)].setIy((Iy[ix][iy-1]+Iy[ix][iy])/2.0);
			}
		}	
	}
	void calc_energy(){
		Ec=0.0;
		for (int ix=0; ix<Nx-1; ix++){
			for (int iy=1; iy<Ny; iy++){
				if (Fx[ix][jm1(iy)]==1.0) Ec+=Math.pow(Qx[ix][jm1(iy)],2.0)/2.0/C;
				Er+=Math.pow(Ix[ix][jm1(iy)],2.0)*R*dt;
			}
		}
		for (int ix=0; ix<Nx; ix++){
			for (int iy=0; iy<Ny; iy++){
				if (Fy[ix][iy]==1.0) Ec+=Math.pow(Qy[ix][iy],2.0)/2.0/C;
				Er+=Math.pow(Iy[ix][iy],2.0)*R*dt;
			}
		}
	}

	double[][] getV(){ return V; }
	double[][] getIx(){ return Ix; }
	double[][] getIy(){ return Iy; }
	double[][] getQx(){ return Qx; }
	double[][] getQy(){ return Qy; }
	double[][] getFx(){ return Fx; }
	double[][] getFy(){ return Fy; }
	double getI0(){ return I0; }
	double getIp(){ return Ip; }
	double getEc(){ return Ec; }
	double getEr(){ return Er; }
}
