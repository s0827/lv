package PT2d;
import java.io.*;
import java.util.*;
/*
 *	テスト用の時系列セル生成消滅の設定と、初期データの取得をひとつのクラスで行う。
 *	時刻0でのセル生成を初期データとする。
 */

public class PT2dpresetdata {
	/**　テスト用データのリスト */	
	LinkedList<PT2dpresetdataitem> datalist;
	LinkedList<PT2dpresetdataitem> initlist;
	public PT2dpresetdata(){
		datalist=new LinkedList<PT2dpresetdataitem>();
		initlist=new LinkedList<PT2dpresetdataitem>();
	}
	public LinkedList<PT2dpresetdataitem> read(String path){
		try {
			BufferedReader in=new BufferedReader(new FileReader(path));
			while(true){
				String line=in.readLine();
				if (line==null) break;
				StringTokenizer tokens=new StringTokenizer(line," \t");
				if (tokens.hasMoreTokens()){
					String com=tokens.nextToken();
//					System.out.print(line+"   ");
					if (com.equals("C") || com.equals("R")){
						PT2dpresetdataitem item=new PT2dpresetdataitem(com,tokens);		//	分解はCellDataItemにやらせる
//						System.out.print(item.getIx()+" "+item.getIy()+" "+item.getIz()+" "+item.getTime());
						datalist.add(item);
						if (com.equals("C") && item.getTime()==0.0e00) {
							initlist.add(item);
						}
					}
					System.out.println();
				}
			}
			in.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
		return datalist;
	}
	public LinkedList<PT2dpresetdataitem> getDatalist(){ return datalist; }
	public LinkedList<PT2dpresetdataitem> getInitlist(){ return initlist; }
}
