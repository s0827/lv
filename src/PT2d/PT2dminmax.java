package PT2d;

public class PT2dminmax {
	double min,max;
	public PT2dminmax(double min, double max){
		this.min=min;
		this.max=max;
	}
	public double get(double d){
		return (d-min)/(max-min);
	}
	public double getMax(){ return max; }
}