package PT2d;

import java.util.*;
import java.text.*;

public class probtest {
	int numcell=100;
	double factor=5.0;
	Random rnd;
	public static void main(String args[]){
		probtest pt=new probtest();
	}
	probtest(){
		rnd=new Random();
		int steps=1000;
		int total=0;
		for (int i=0; i<steps; i++){
			System.out.print(i+">");
			total+=generate();
		}
		System.out.println("total= "+total+"  average= "+new DecimalFormat("0.0000").format((double)total/(double)steps));
	}
	int generate(){
		TreeMap<Double, Integer> map=new TreeMap<Double, Integer>();
		double pmax=create_map(map);
		int np=(int)pmax+1;
		int ic=0;
		for (int i=0; i<np; i++){
			SortedMap<Double, Integer> tail=map.tailMap(new Double(rnd.nextDouble()+(double)i));
			if (tail.size()>0) {
				System.out.print("  ");
				System.out.print(new DecimalFormat("0.00000").format(tail.firstKey()));
				int I=tail.get(tail.firstKey());
				System.out.print(" "+I);
				ic++;
			}
		}
		System.out.println("  ("+ic+")");
		return ic;
	}
	double create_map(TreeMap<Double, Integer> map){
		double p=0.0;
		double f=factor/(double)numcell;
		for (int n=0; n<numcell; n++) {
			p+=rnd.nextDouble()*f;
			map.put(new Double(p), new Integer(n));
		}
		return p;
	}
}
