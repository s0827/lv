package PT2d;

import java.awt.*;
import java.util.Calendar;

public class PT2dframe extends Frame {
	PT2dpanel pl;
	public PT2dframe(){
		setSize(PT2dparms.framewidth, PT2dparms.frameheight);
		setVisible(true);		
		setLayout(null);
		pl=new PT2dpanel();
		add(pl);
		pl.setLocation(PT2dparms.px, PT2dparms.py);			
	}
/** グラフィクス画面表示内容のファイルへの保存のため、グラフィックス描画用クラスを取得して返却する。
*/
	public PT2drenderer setup_renderer(PT2dparms parms, PT2dcell[][] cells){		
		return pl.setup_renderer(parms,cells);
	}
/**	グラフィクス画面の描画を実行する。 */
	public void display(){
		repaint();
		pl.repaint();
	}
/** 描画を許可する。 */	
	public void enable_paint_panel(){ pl.enable_paint_panel(); }
/** 描画を禁止する。 */
	public void disable_paint_panel(){ pl.disable_paint_panel(); }
/**　paintメソッド */	
	public void paint(Graphics gc){
		gc.setColor(Color.black);
		gc.fillRect(0,0,PT2dparms.framewidth,PT2dparms.frameheight);
		Calendar calendar=Calendar.getInstance();
		gc.setColor(Color.white);	
		gc.drawString(calendar.getTime().toString(),0,PT2dparms.frameheight-10);
	}
}
