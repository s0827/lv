package PT2d;

public class PT2dcalcdata extends PT2dabstractd {
	PT2dparms parms;
	int Nx,Ny;
	double[][] V,Qx,Qy,Rx,Ry;			//	計算する回路のパラメータ
	double[][] V0,Qx0,Qy0,Rx0,Ry0;		//	前ステップの値
	double[][] Cx,Cy;
	double Ec,Er;						//	コンデンサの蓄積エネルギー、抵抗の消費エネルギー
	double I0,Ip;						//	極板の電流
	double Vp;
	double dt;

	PT2dcalcdata(){}
	void initialize_arrays(){
		Nx=parms.getNx();
		Ny=parms.getNy();
		Vp=parms.getVp();

		V=new double[Nx][Ny-2];
		Qx=new double[Nx-1][Ny-2];
		Qy=new double[Nx][Ny-1];
		Rx=new double[Nx-1][Ny-2];
		Ry=new double[Nx][Ny-1];

		V0=new double[Nx][Ny-2];
		Qx0=new double[Nx-1][Ny-2];
		Qy0=new double[Nx][Ny-1];
		Rx0=new double[Nx-1][Ny-2];
		Ry0=new double[Nx][Ny-1];

		Cx=new double[Nx-1][Ny-2];
		Cy=new double[Nx][Ny-1];
		
		Ec=0.0;
		Er=0.0;
		I0=0.0;
		Ip=0.0;
	
		clear(V);
		clear(Rx);
		clear(Ry);
		clear(Qx);
		clear(Qy);

		clear(V0);
		clear(Rx0);
		clear(Ry0);
		clear(Qx0);
		clear(Qy0);

		clear(Cx);
		clear(Cy);
	}
	void shift(){
		shift(V,V0);
		shift(Rx,Rx0);
		shift(Ry,Ry0);
		shift(Qx,Qx0);
		shift(Qy,Qy0);
	}

	void calc_field(PT2dcell[][] cells){
		double dx=parms.getDx();
		double dy=parms.getDy();
		for (int ix=0; ix<Nx; ix++){
			for (int iy=0; iy<Ny-2; iy++) cells[ix][iy].setV(V[ix][iy]);
		}
		double[][] Ex=calc_Ex();
		double[][] Ey=calc_Ey();
		
		double[][] IRx=calc_IRx();
		double[][] IRy=calc_IRy();
		double[][] ICx=calc_ICx();
		double[][] ICy=calc_ICy();
		
		set_EIx(cells,Ex,IRx,ICx);
		set_EIy(cells,Ey,IRy,ICy);
		
		I0=0.0;
		Ip=0.0;
		for (int ix=0; ix<Nx; ix++) {
			I0+=IRy[ix][0]+ICy[ix][0];
			Ip+=IRy[ix][Ny-2]+ICy[ix][Ny-2];
//			I0+=IRy[ix][0];
//			Ip+=IRy[ix][Ny-2];
		}
		calc_energy(IRx,IRy);
	}
	double[][] calc_Ex(){
		double[][] Ex=new double[Nx-1][Ny-2];
		double dx=parms.getDx();
		for (int ix=0; ix<Nx-1; ix++){
			for (int iy=1; iy<Ny-1; iy++){
				Ex[ix][jm1(iy)]=(V[ix+1][jm1(iy)]-V[ix][jm1(iy)])/dx;
			}
		}
		return Ex;
	}
	double[][] calc_Ey(){
		double [][] Ey=new double[Nx][Ny-1];
		double dy=parms.getDy();
		for (int ix=0; ix<Nx; ix++){
			Ey[ix][0]=V[ix][jm1(1)]/dy;
			Ey[ix][Ny-2]=Vp-V[ix][jm1(Ny-2)]/dy;
			for (int iy=1; iy<Ny-2; iy++){
				Ey[ix][iy]=(V[ix][jm1(iy+1)]-V[ix][jm1(iy)])/dy;
			}
		}
		return Ey;
	}	
	void set_EIx(PT2dcell[][] cells, double[][] Ex, double[][] IRx, double[][] ICx){
		for (int iy=1; iy<Ny-1; iy++){
			cells[0][jm1(iy)].setEx(Ex[0][jm1(iy)]);
			cells[0][jm1(iy)].setIx(IRx[0][jm1(iy)]+ICx[0][jm1(iy)]);
			for (int ix=1; ix<Nx-1; ix++){
				cells[ix][jm1(iy)].setEx((Ex[ix-1][jm1(iy)]+Ex[ix][jm1(iy)])/2.0);
				cells[ix][jm1(iy)].setIx((IRx[ix-1][jm1(iy)]+ICx[ix-1][jm1(iy)]+IRx[ix][jm1(iy)]+ICx[ix][jm1(iy)])/2.0);
			}
		}
	}
	void set_EIy(PT2dcell[][] cells, double[][] Ey, double[][] IRy, double[][] ICy){
		for (int ix=0; ix<Nx; ix++){
			cells[ix][jm1(1)].setEy(Ey[ix][jm1(1)]);
			cells[ix][jm1(1)].setIy(IRy[ix][jm1(1)]+ICy[ix][jm1(1)]);
			cells[ix][jm1(Ny-2)].setEy(Ey[ix][jm1(Ny-2)]);
			cells[ix][jm1(Ny-2)].setIy(IRy[ix][jm1(Ny-2)]+ICy[ix][jm1(Ny-2)]);
			for (int iy=2; iy<Ny-2; iy++){
				cells[ix][jm1(iy)].setEy((Ey[ix][jm1(iy-1)]+Ey[ix][jm1(iy)])/2.0);
				cells[ix][jm1(iy)].setIy((IRy[ix][jm1(iy-1)]+ICy[ix][jm1(iy-1)]+IRy[ix][jm1(iy)]+ICy[ix][jm1(iy)])/2.0);
			}
		}
	}
	double[][] calc_IRx(){
		double[][] IRx=new double[Nx-1][Ny-2];
		for (int ix=0; ix<Nx-2; ix++){
			for (int iy=1; iy<Ny-1; iy++) {
				IRx[ix][jm1(iy)]=(V[ix+1][jm1(iy)]-V[ix][jm1(iy)])/Rx[ix][jm1(iy)];
			}
		}
		return IRx;
	}
	double[][] calc_IRy(){
		double[][] IRy=new double[Nx][Ny-1];
		for (int ix=0; ix<Nx; ix++){
			IRy[ix][0]=V[ix][jm1(1)]/Ry[ix][jm1(1)];
			IRy[ix][Ny-2]=(Vp-V[ix][jm1(Ny-2)])/Ry[ix][Ny-2];
			for (int iy=1; iy<Ny-2; iy++){
				IRy[ix][iy]=(V[ix][jm1(iy+1)]-V[ix][jm1(iy)])/Ry[ix][iy];
			}			
		}
		return IRy;
	}
	double[][] calc_ICx(){
		double[][] ICx=new double[Nx-1][Ny-2];
		for (int ix=0; ix<Nx-2; ix++){
			for (int iy=1; iy<Ny-1; iy++){
				ICx[ix][jm1(iy)]=(Qx[ix][jm1(iy)]-Qx0[ix][jm1(iy)])/dt;
			}
		}
		return ICx;
	}
	double[][] calc_ICy(){
		double[][] ICy=new double[Nx][Ny-1];
		for (int ix=0; ix<Nx; ix++){
			for (int iy=0; iy<Ny-1; iy++){
				ICy[ix][iy]=(Qy[ix][iy]-Qy0[ix][iy])/dt;
			}
		}
		return ICy;
	}
	void calc_energy(double[][] IRx, double[][] IRy){
		Ec=0.0;
		for (int ix=1; ix<Nx-1; ix++){
			for (int iy=1; iy<Ny-1; iy++){
				Ec+=Math.pow(Qx[ix][jm1(iy)],2.0)/2.0/Cx[ix][jm1(iy)];
				Er+=Math.pow(IRx[ix][jm1(iy)],2.0)*Rx[ix][jm1(iy)]*dt;
			}
		}
		for (int ix=0; ix<Nx; ix++){
			for (int iy=0; iy<Ny-1; iy++){
				Ec+=Math.pow(Qy[ix][iy],2.0)/2.0/Cy[ix][iy];
				Er+=Math.pow(IRy[ix][iy],2.0)*Ry[ix][iy]*dt;
			}
		}
	}
	int jm1(int j){ return j-1; }			//	配列a,bに値をpack,unpackする時に使う
	
	double[][] getV(){ return V; }
	double[][] getQx(){ return Qx; }
	double[][] getQy(){ return Qy; }
	double[][] getRx(){ return Rx; }
	double[][] getRy(){ return Ry; }
	double[][] getCx(){ return Cx; }
	double[][] getCy(){ return Cy; }
	double getEr(){ return Er; }
	double getEc(){ return Ec; }
	double getI0(){ return I0; }
	double getIp(){ return Ip; }
}
