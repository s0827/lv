package PT2d;

import java.text.DecimalFormat;
import java.util.*;
import PT2dtools.FDcount;

public class GenSF6 extends PT2dgenerator {
	double e0=9.68e6;		//	基準電界 [V/m]	計算は1atm限定とする。
	double rate0=3.5e8;		//	基準レート[/s]
	double efmin=0.01;		//	再結合の抑制において、最大電界を基準電界のefmin倍までで制限
	GenSF6(){
	}
	void create_cell(double dt, int i){
		e0=parms.getE0();
		rate0=parms.getRate0();
		efmin=parms.getEfmin();
		double rate=rate0*parms.getMult();
		double prob=dt*rate;
		double rprob=dt*rate;
		TreeMap<Double, PT2dcell> crtree=new TreeMap<Double, PT2dcell>();
		double crprob=create_tree(prob,crtree);
		LinkedList<PT2dcell> crcells=get_cells(crtree,crprob);				//	1ステップで複数セルの生成可
		checkcells("C",crcells);
		TreeMap<Double, PT2dcell> rmtree=new TreeMap<Double, PT2dcell>();
		double rmprob=remove_tree(rprob,rmtree);			
		LinkedList<PT2dcell> rmcells=get_cells(rmtree,rmprob);
		checkcells("R",rmcells);

		cluster.out("  step "+i+" create p="+new DecimalFormat("0.00").format(crprob)+" remove p="+new DecimalFormat("0.00").format(rmprob));
		if (crcells.size()>0) {
			cluster.outnocr("  (C "+crcells.size());
			for (PT2dcell crcell: crcells){
				cells[crcell.getX()][crcell.getY()].ionize();
				Cell c=new Cell(crcell.getX(),crcell.getY(),0);
				cluster.add_cell(c);
			}
			cluster.out(")");
		}
		if (rmcells.size()>0){
			cluster.outnocr("  (R "+rmcells.size());
			for (PT2dcell rmcell: rmcells){
				cells[rmcell.getX()][rmcell.getY()].recombine();
				Cell c=cluster.get_cell(rmcell.getX(),rmcell.getY(),0);
				cluster.remove_cell(c);				
			}
			cluster.out(")");
		}
		cluster.out("");
		if (crcells.size()>0 || rmcells.size()>0) cluster.output_cluster(i);		
	}
	double create_tree(double prob, TreeMap<Double, PT2dcell> tree){
		double total=0.0;
		for (int ix=0; ix<cells.length; ix++){					//	正規化された分布関数を得るために、最初に相対的な確率の総和を取る。
			for (int iy=0; iy<cells[ix].length; iy++){
				PT2dcell cell=cells[ix][iy];
				if (cell.is_neutral()) {
					double tp=prob;
					if (parms.getEf_f()){
						double efield=cell.getE();
						if (efield<=0.0) efield=0.0;
						tp=prob*(efield/e0);
						if (tp>1.0) tp=1.0;						
					}
					total+=tp;					
					tree.put(new Double(total),cell);		
				}
			}
		}
		return total;
	}
	double remove_tree(double rprob, TreeMap<Double, PT2dcell> tree){
		double total=0.0;
		for (int ix=0; ix<cells.length; ix++){					//	正規化された分布関数を得るために、最初に相対的な確率の総和を取る。
			for (int iy=0; iy<cells[ix].length; iy++){
				PT2dcell cell=cells[ix][iy];
				if (cell.is_plasma()) {
					double rp=0.0;
					if (parms.getRecomb_f()){
						if (parms.getEf_f()){
							double efield=cell.getE();
							if (efield<=0.0) efield=0.0;
							double efact=efield/e0;
							if (efact<efmin) efact=efmin;
							rp=rprob/efact;
						}
						else {
							rp=rprob*parms.getR_factor();
						}
					}
					if (parms.getCr_f()){
						rp=rp/(1.0+cell.getI()/parms.getJ0());
					}
					if (rp>1.0) rp=1.0;
					total+=rp;
					tree.put(new Double(total),cell);		
				}
			}
		}
		return total;
	}
//	デバッグ用
	void checktree(String cr, TreeMap<Double, PT2dcell> tree){
		TreeMap<String, FDcount> map=new TreeMap<String,FDcount>();
		Iterator<Double> it=tree.keySet().iterator();
		while(it.hasNext()){
			PT2dcell cell=tree.get(it.next());
			int ix=cell.getX();
			int iy=cell.getY();
			String idx=new DecimalFormat("00").format(ix)+new DecimalFormat("00").format(iy);
			FDcount C=map.get(idx);
			if (C==null){
				map.put(idx,new FDcount());
			}
			else {
				C.incr();
			}
		}
		output(cr,map);
	}
	void checkcells(String cr, LinkedList<PT2dcell> cells){
		TreeMap<String, FDcount> map=new TreeMap<String,FDcount>();
		for (PT2dcell cell: cells){
			int ix=cell.getX();
			int iy=cell.getY();
			String idx=new DecimalFormat("00").format(ix)+new DecimalFormat("00").format(iy);
			FDcount C=map.get(idx);
			if (C==null){
				map.put(idx,new FDcount());
			}
			else {
				C.incr();
			}
		}
		output(cr,map);
	}
	void output(String cr,TreeMap<String, FDcount> map){
		Iterator<String> it=map.keySet().iterator();
		while(it.hasNext()){
			String key=it.next();
			FDcount C=map.get(key);
			if (C.getCount()>1) {
				System.out.println("  Error: Dup "+cr+" "+key);
			}
		}
	}
}
