package PT2d;

import java.awt.*;

public class PT2dpanel extends Panel {
	PT2drenderer renderer;
	boolean paint_panel_f=false;
	PT2dpanel(){
		setSize(PT2dparms.panelwidth, PT2dparms.panelheight);
	}
	PT2drenderer setup_renderer(PT2dparms parms, PT2dcell[][] cells){
		renderer=new PT2drenderer(parms, cells);
		return renderer;
	}
	/** 描画を許可する。*/
	void enable_paint_panel(){ paint_panel_f=true; }
/** 描画を禁止する。*/
	void disable_paint_panel(){ paint_panel_f=false; }
	public void paint(Graphics gc){
		if (!paint_panel_f) return;
		Graphics2D g2=(Graphics2D)gc;
		renderer.draw(g2);
	}
}
