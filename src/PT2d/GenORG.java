package PT2d;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.TreeMap;

public class GenORG extends PT2dgenerator {
	GenORG(){
		
	}
	void create_cell(double dt,int i){
		boolean crflag=false;
		double irate=1.0/parms.getIonization_time();
		double rrate=(1.0-parms.getSteady_pop())/parms.getSteady_pop()*irate;
		double prob=dt*irate;
		double rprob=dt*rrate;
		TreeMap<Double, PT2dcell> crtree=new TreeMap<Double, PT2dcell>();
		double crprob=create_tree(prob,crtree);
		LinkedList<PT2dcell> crcells=get_cells(crtree,crprob);		//	1ステップで複数セルの生成可
		boolean crstat=false;
		System.out.print("  create p="+new DecimalFormat("0.00").format(crprob));
		if (crcells.size()>0) {
			crstat=true;
			System.out.print(" "+crcells.size());
			for (PT2dcell crcell: crcells){
				cells[crcell.getX()][crcell.getY()].ionize();
				Cell c=new Cell(crcell.getX(),crcell.getY(),0);
				cluster.add_cell(c);
				System.out.print(" ["+crcell.getX()+","+crcell.getY()+"]");
			}
		}
		if (parms.getRecomb_f()){
			TreeMap<Double, PT2dcell> rmtree=new TreeMap<Double, PT2dcell>();
			double rmprob=remove_tree(rprob,rmtree);			
			LinkedList<PT2dcell> rmcells=get_cells(rmtree,rmprob);
			System.out.print("  remove p="+new DecimalFormat("0.00").format(rmprob));
			if (rmcells.size()>0){
				crstat=true;
				System.out.print(" "+rmcells.size());
				for (PT2dcell rmcell: rmcells){
					cells[rmcell.getX()][rmcell.getY()].recombine();
					Cell c=cluster.get_cell(rmcell.getX(),rmcell.getY(),0);
					cluster.remove_cell(c);				
					System.out.print(" ["+rmcell.getX()+","+rmcell.getY()+"]");
				}				
			}
		}
		System.out.println();
		if (crstat) cluster.output_cluster(i);
	}
	double create_tree(double prob, TreeMap<Double, PT2dcell> tree){
		double total=0.0;
		for (int ix=0; ix<cells.length; ix++){					//	正規化された分布関数を得るために、最初に相対的な確率の総和を取る。
			for (int iy=0; iy<cells[ix].length; iy++){
				PT2dcell cell=cells[ix][iy];
				if (cell.is_neutral()) {
					if (parms.getEf_f()) {
						double efield=cell.getE();
						if (efield<=0.0) efield=1.0;
						total+=Math.pow(efield/parms.getE0(),parms.getEf_factor())*prob;
					}
					else {
						total+=prob;
					}
					tree.put(new Double(total),cell);		
				}
			}
		}
		return total;
	}
	double remove_tree(double prob, TreeMap<Double, PT2dcell> tree){
		double total=0.0;
		for (int ix=0; ix<cells.length; ix++){					//	正規化された分布関数を得るために、最初に相対的な確率の総和を取る。
			for (int iy=0; iy<cells[ix].length; iy++){
				PT2dcell cell=cells[ix][iy];
				if (cell.is_plasma()) {
					if (parms.getCr_f()){
						double current=cell.getI();
//						current=Math.max(current,1.0e-3);
						total+=prob/(1.0+Math.pow(current/parms.getJ0(),parms.getCr_factor()));						
					}
					else {
						total+=prob;
					}
					tree.put(new Double(total),cell);							
				}
			}
		}
		return total;
	}
}
