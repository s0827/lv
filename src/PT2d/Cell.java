package PT2d;

import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.LinkedList;

/**	プラズマ化したセルの情報を保持し、接続状態の更新の処理を行なう。<br>
 */
public class Cell {
/** セルのx座標 */			int ix;
/** セルのy座標 */			int iy;
/** セルのz座標 */			int iz;
/** セルの識別番号 */		int id;
/** 親セル */				Cell parent;
/** 子セルのリスト */		LinkedList<Cell> childs;
int MAXCELLS=100;

	Cell(){}
	public Cell(int ix, int iy, int iz){
		initialize(ix,iy,iz);
	}
	void initialize(int ix, int iy, int iz){
		this.ix=ix;
		this.iy=iy;
		this.iz=iz;
		id=MAXCELLS*MAXCELLS*ix+MAXCELLS*iy+iz;
		clear_connection();
	}
	void clear_connection(){
		parent=null;
		childs=new LinkedList<Cell>();		
	}
	/** 子セルをリストに追加する。
	* @param child 追加される子セル
	*/
	public void add_child(Cell child){
		childs.add(child);
	}
	/** クラスタの先頭セルを取得する。
	* @return 当該セルが所属するクラスタの先頭Cellを返す。
	*/
	Cell get_head(){
		Cell head=this;
		if (parent!=null) head=parent.get_head();
		return head;
	}
	/** 当該セルがクラスタの先頭セルかどうか問い合わせる。
	* @return 当該セルがクラスタの先頭であれば真を返す。
	*/
	boolean is_head(){
		boolean status=false;
		if (parent==null) status=true;
		return status;
	}
	/** 入力された子セルがリストにあれば削除する。
	* @param child 削除される子セル
	*/
	void remove_child(Cell child){
		if (childs.contains(child)) childs.remove(child);
	}
	/** 親セルを削除する。*/
	void remove_parent(){
		parent=null;
	}
	/** セルが追加された時に接続状況を更新する。入力セルがこれまで子セルの一つだった時、それを親セルとする。それまでの親セルの先頭まで再帰的に処理する。
	* @param new_parent 新しい親セル
	*/
	void connect(Cell new_parent){									//	親ノードと子ノードを入れ替える
		remove_child(new_parent);
		if (parent!=null){
			Cell old_parent=parent;
			childs.add(old_parent);
			old_parent.connect(this);
		}
		parent=new_parent;
	}
	boolean find_reconnection(Cell[][][] cells, Cell head, int nx, int ny, int nz){
		boolean status=false;
		LinkedList<Cell> neighbors=new LinkedList<Cell>();
		if (ix<nx-1) if (cells[ix+1][iy][iz]!=null) neighbors.add(cells[ix+1][iy][iz]);
		if (ix>0)    if (cells[ix-1][iy][iz]!=null) neighbors.add(cells[ix-1][iy][iz]);
		if (iy<ny-1) if (cells[ix][iy+1][iz]!=null) neighbors.add(cells[ix][iy+1][iz]);
		if (iy>0)    if (cells[ix][iy-1][iz]!=null) neighbors.add(cells[ix][iy-1][iz]);
		if (iz<nz-1) if (cells[ix][iy][iz+1]!=null) neighbors.add(cells[ix][iy][iz+1]);
		if (iz>0)    if (cells[ix][iy][iz-1]!=null) neighbors.add(cells[ix][iy][iz-1]);
		LinkedList<Cell> connected=new LinkedList<Cell>();
		if (parent!=null) connected.add(parent);
		connected.addAll(childs);							//	接続状態を調べるべきセルを取得
		neighbors.removeAll(connected);						//	すでに接続状態にあるセル以外に接しているセルがあるか調べる
		if (neighbors.size()>0){
			for (Cell neighbor: neighbors){
				if (head.id!=neighbor.get_head().id){
					neighbor.add_child(this);
					parent=neighbor;
					status=true;
					break;
				}
			}
		}
		else {
			for (Cell child: childs){				
				if (child.find_reconnection(cells,head,nx,ny,nz)){
					Cell new_parent=child;
					remove_child(new_parent);
					child.add_child(this);
					parent=new_parent;					
	//				System.out.println(id+" new parent="+parent.id);
					status=true;
					break;
				}
			}			
		}
	//	System.out.println(id+" "+status);
		return status;
	}
	/** クラスタの接続状態をコンソールに出力する。フラグが偽の時は、出力せず、セル数を返却する。
	* @param flag コンソール出力するかどうかのフラグ
	* @return 全セル数
	*/
	public int output_cluster(PrintWriter out, boolean flag){
		int sum=1;
		if (flag) print(out," "+new DecimalFormat("000000").format(id));
	//	if (parent!=null) out.print("("+new DecimalFormat("000000").format(parent.id)+")");
		if (childs.size()>0) {
			if (flag) if (childs.size()>1) print(out," (");
			int i=0;
			for (Cell cell:childs) {
				sum+=cell.output_cluster(out,flag);
				if (flag) if (childs.size()>1 && (i<childs.size()-1)) print(out," |");
				i++;
			}
			if (flag) if (childs.size()>1) print(out," )");			
		}
		return sum;
	}
	public void print(PrintWriter out, String str){
		if (out==null){
			System.out.print(str);
		}
		else {
			out.print(str);
		}
	}
	public String output_cluster(){
		String str="";
		str+=" "+new DecimalFormat("000000").format(id);
		if (childs.size()>0){
			if (childs.size()>1) str+=" (";
			int i=0;
			for (Cell cell: childs){
				str+=cell.output_cluster();
				if (childs.size()>1 && (i<childs.size()-1)) str+=" |";
				i++;
			}
			if (childs.size()>1) str+=" )";
		}
		return str;
	}
	public LinkedList<Cell> get_cell_set(){
		LinkedList<Cell> set=new LinkedList<Cell>();
		set.add(this);
		for (Cell cell: childs) set.addAll(cell.get_cell_set());
		return set;
	}
	public void setParent(Cell parent){ this.parent=parent; }
	public int getId(){ return id; }
	public int getIx(){ return ix; }
	public int getIy(){ return iy; }
	public int getIz(){ return iz; }
	public Cell getParent(){ return parent; }
	LinkedList<Cell> getChilds(){ return childs; }
}
