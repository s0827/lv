package PT2d;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class PT2dmonitor implements Runnable {
	/** parameters to control graphics */	PT2dparms parms;
	/** true for execute */					boolean run_f=false;
	/** true for wait	 */					boolean wait_f=false;
	/** true for step execution */			boolean proceed_f=true;
	/**	true to stop calculation */			boolean stop_f=false;
	/** number of steps to proceed */		int istep=0;
	PT2dmonitor(PT2dparms parms){
		this.parms=parms;
	}
	void help(){
		System.out.println();
		System.out.println("list of commands");
		System.out.println("G : go");
		System.out.println("S n : stop execution or proceed n steps");
		System.out.println("C : display cells");
		System.out.println("I : display current");
		System.out.println("E : display e-filed");
		System.out.println("V : eisplay voltage");
		System.out.println("Q : quit");
	}
/** Main event loop for the present thread, to process commands */
	public void run(){
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		proceed_f=true;
		while(true){
			try {
				String line=br.readLine();
				StringTokenizer tokens=new StringTokenizer(line," \n");
				if (tokens.hasMoreTokens()){
					String word=tokens.nextToken();
					if (word.toUpperCase().equals("G")) {
						run_f=true;
						send_message();
					}
					else if (word.toUpperCase().equals("S")) {
						run_f=false;
						istep=1;
						if (tokens.hasMoreTokens()) istep=Integer.parseInt(tokens.nextToken());
						send_message();
					}
					else if (word.toUpperCase().equals("H")) help();
					else if (word.toUpperCase().equals("C")) {			//	redraw commands, without proceeding execution
						parms.display_cell();
						proceed_f=false;
						send_message();
					}
					else if (word.toUpperCase().equals("I")) {
						parms.display_current();
						proceed_f=false;
						send_message();
					}
					else if (word.toUpperCase().equals("E")) {
						parms.display_efield();
						proceed_f=false;
						send_message();
					}
					else if (word.toUpperCase().equals("V")) {
						parms.display_volt();
						proceed_f=false;
						send_message();
					}
					else if (word.toUpperCase().equals("Q")) {
						stop_f=true;
					}
				}
			}
			catch(Exception e){ System.err.println(e); }		
		}
	}
/**  restart run function */
	synchronized void send_message(){
		if (wait_f) {
			if (istep>0) istep--;
			notifyAll();
		}
	}
/** to determine whether proceed execution or not */
	synchronized boolean monitor(){
		if (run_f) return true;
		if (istep>0){
			istep--;
			return true;
		}
		try {
			System.out.print("program halted > ");
			wait_f=true;
			wait();
		}
		catch(Exception e){ System.err.println(e); }
		return proceed_f;
	}
	boolean isStop_f(){ return stop_f; }
}
