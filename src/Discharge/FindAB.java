package Discharge;

import java.io.*;
import java.util.*;
import java.text.*;
import java.awt.*;

public class FindAB extends Frame {
	double Amin=100.0, Amax=100000.0;
	double Bmin=50.0,  Bmax=100.0;
	double gamma=1.0e-6;
	double dmax=3.0;
	int nratio=2;
	int numd=200;
	int panelwidth=numd*nratio, panelheight=numd*nratio;
	FindABpanel panel;
	public static void main(String args[]){
		FindAB fab=new FindAB();
	}
	FindAB(){
		try {
			initialize_graphics();
			TreeMap<Double, Double> data=read_data();
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			String line;
			String[] words;
			System.out.print("Enter Amin, Amax ("+new DecimalFormat("0").format(Amin)+","+new DecimalFormat("0").format(Amax)+") = ");
			line=in.readLine();
			if (line.length()>0){
				words=line.split(",");
				Amin=Double.parseDouble(words[0]);
				Amax=Double.parseDouble(words[1]);
			}
			System.out.print("Enter Bmin, Bmax ("+new DecimalFormat("0").format(Bmin)+","+new DecimalFormat("0").format(Bmax)+") = ");
			line=in.readLine();
			if (line.length()>0){
				words=line.split(",");
				Bmin=Double.parseDouble(words[0]);
				Bmax=Double.parseDouble(words[1]);
			}
			System.out.print("Enter gamma (1.0e-6) = ");
			line=in.readLine();
			if (line.length()>0){
				gamma=Double.parseDouble(line);
			}
			calculate(data);
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	TreeMap<Double, Double> read_data(){
		TreeMap<Double, Double> data=new TreeMap<Double, Double>();
		try {
			BufferedReader in=new BufferedReader(new FileReader("sf6.txt"));
			while(true){
				String line=in.readLine();
				if (line==null) break;
				String[] words=line.split("\t");
				Double PS=new Double(words[0]);
				Double UD=new Double(words[1]);
				data.put(PS,UD);
//				System.out.println(PS+"\t"+UD);
			}
			in.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
		return data;
	}
	void calculate(TreeMap<Double, Double> data){
		double[][] d=new double[numd][numd];
		double dA=Math.pow(Amax/Amin,1/(double)numd);
		double dB=Math.pow(Bmax/Bmin,1/(double)numd);
		double emin=10000.0;
		int imin=0;
		int jmin=0;
		for (int i=0; i<numd; i++){
			double A=Amin*Math.pow(dA,(double)i);
			for (int j=0; j<numd; j++){
				double B=Bmin*Math.pow(dB,(double)j);
				Iterator<Double> it=data.keySet().iterator();
				double error=0.0;
				int nd=0;
				while(it.hasNext()){
					Double PS=it.next();
					double ps=PS.doubleValue();
					double ud=data.get(PS).doubleValue();
					double Ud=B*ps/(Math.log(A*ps/(Math.log(1.0+1.0/gamma))));
					if (Ud>0.0 && ps>0.04){
//						error+=Math.pow((Ud-ud)/ud,2.0);
						error+=Math.pow(Ud-ud,2.0);
						nd++;
					}
				}
				error=Math.sqrt(error)/(double)nd;
				d[i][j]=error;
//				System.out.println(A+"\t"+B+"\t"+error);
				if (d[i][j]<emin){
					imin=i;
					jmin=j;
					emin=error;
				}
			}
		}
		double A=Amin*Math.pow(dA,(double)imin);
		double B=Bmin*Math.pow(dB,(double)jmin);
		System.out.println("A= "+new DecimalFormat("0.0").format(A)+"  B="+new DecimalFormat("0.0").format(B)
				+"  error= "+new DecimalFormat("0.00000").format(emin));
		panel.setD(d);
		panel.set_mins(imin,jmin);
		repaint();
	}
	void initialize_graphics(){
		setSize(panelwidth,panelheight);
		setVisible(true);
		setLayout(null);
		panel=new FindABpanel(panelwidth,panelheight);
		panel.setLocation(0,0);
		add(panel);
		panel.setNratio(nratio);
		panel.setDmax(dmax);
	}
	public void paint(Graphics gc){
		if (panel!=null) panel.repaint();
	}
}
