package Discharge;

import java.awt.*;

public class FindABpanel extends Panel {
	int panelwidth,panelheight;
	double[][] d;
	double dmax;
	int nratio;
	int imin,jmin;
	FindABpanel(int panelwidth, int panelheight){
		this.panelwidth=panelwidth;
		this.panelheight=panelheight;
		setSize(panelwidth,panelheight);
	}
	public void paint(Graphics gc){
		int numd=panelwidth/nratio;
		if (d==null) return;
		System.out.println("drawing");
		for (int i=0; i<numd; i++){
			for (int j=0; j<numd; j++){
				double y=d[i][j]/dmax;
				if (y>1.0) y=1.0;
				if (y<0.0) y=0.0;
				int r=(int)((1.0-y)*255.0);
				int g=(int)((1.0-y)*255.0);
				int b=(int)((1.0-y)*255.0);
				gc.setColor(new Color(r,g,b));
				gc.fillRect(i*nratio,j*nratio,nratio,nratio);
			}
		}
		gc.setColor(new Color(255,255,0));
		gc.fillRect(imin*nratio,jmin*nratio,nratio,nratio);
	}
	void setD(double[][] d){ this.d=d; }
	void setDmax(double dmax){ this.dmax=dmax;	}
	void setNratio(int nratio){ this.nratio=nratio; }
	void set_mins(int imin, int jmin){
		this.imin=imin;
		this.jmin=jmin;
	}
}
